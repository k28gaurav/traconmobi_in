package com.traconmobi.tom;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.traconmobi.tom.model.UserTravelDetails;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

/**
 * Created by kumargaurav on 8/4/16.
 */
public class DemoLocationListener extends Service {
    public static String TAG = "DemoLocationListener";
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 0; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 0; // in Milliseconds
    protected LocationManager locationManager;
    MyLocationListener listener;
    SessionManager session;
    CouchBaseDBHelper couchDb = null;
    private static String startTripLocation;
    private static String startTripTime;
    private static String transportUsed;
    private static String userName;
    private static String companyId;
    private static String userId;
    private UserTravelDetails userTravelDetails;
    private static int tripCount;
    private static String tripKey = "Trip";
    private static String timezone;
    private static String locationId;


    @Override
    public void onCreate() {
        try {
             /* Use the LocationManager class to obtain GPS locations */
            Log.e(TAG, "Service onCreate");

            session = new SessionManager(getApplicationContext());
            couchDb = new CouchBaseDBHelper(getApplicationContext());
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, "Exception onCreate" + e.getMessage());
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent myIntent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent myIntent, int flags, int startId) {

        Log.i(TAG, "Service onStartCommand");
        startTripLocation = myIntent.getStringExtra("startLoc");
        transportUsed = myIntent.getStringExtra("transport_val");
        startTripTime = myIntent.getStringExtra("starttime");
        userName = myIntent.getStringExtra("username");
        companyId = myIntent.getStringExtra("companyId");
        userId = myIntent.getStringExtra("userId");
        tripCount = myIntent.getIntExtra("trip_count", 1);
        locationId = myIntent.getStringExtra("locationId");
        timezone = myIntent.getStringExtra("timezone");

        Log.e(TAG, "Intent data: " + startTripLocation + ":" + transportUsed + ":" + startTripTime + ":" + userName + ":" +
                companyId + ":" + userId + "count" + tripCount);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new MyLocationListener();

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                MINIMUM_TIME_BETWEEN_UPDATES,
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                listener
        );

        //Creating new thread for my service
        //Always write your long running tasks in a separate thread, to avoid ANR
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Service onDestroy");
        locationManager.removeUpdates(listener);
        locationManager = null;
    }

    private class MyLocationListener implements LocationListener {
        double prevLat;
        double prevLng;
        boolean flag = true;
        float[] result = new float[3];

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String locUpdateTime;
        float distance;
        int batterLevel;
        String message;
        String gps;
        String docId;

        List<com.traconmobi.tom.model.Location> locationListDetails = new ArrayList<com.traconmobi.tom.model.Location>();
        com.traconmobi.tom.model.Location locationDetails;
        Map<String, List<com.traconmobi.tom.model.Location>> tripwiseWaypoint = new HashMap<String, List<com.traconmobi.tom.model.Location>>();
        Map<String, UserTravelDetails> userTravelDetailsMap = new HashMap<String, UserTravelDetails>();

        public void onLocationChanged(Location location) {
            Date curDate = new Date();
            locUpdateTime = dateFormat.format(curDate).toString();
            batterLevel = PowerConnectionReceiver.level;


            if (flag) {
                Log.e(TAG, "First Location changed");
                setPrevLat(location.getLatitude());
                setPrevLng(location.getLongitude());
                setDistance(0);
                docId = userName +"_" + companyId;
                session.setDistance(0);
                gps = String.valueOf(location.getLatitude()) + ":" + String.valueOf(location.getLongitude()) + ":" + getDistance();
                session.setText(gps);
                try {
                    tripwiseWaypoint = couchDb.getWaypoint(userId, companyId);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }

                try {
                    userTravelDetailsMap = couchDb.getUserTrackHistory(userId, companyId);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
                flag = false;
            } else {
                message = String.format("Trip:%1$s,Latitude:%2$s,Longitude:%3$s,Distance:%4$s,Time%5$s,",
                        tripCount, location.getLongitude(), location.getLatitude(), getDistance(), locUpdateTime);
                Log.e(TAG, message + "docId: " + docId);
                try {
                    couchDb.updateDoc(userName, String.valueOf(roundToDecimals(location.getLatitude(),6)), String.valueOf(roundToDecimals(location.getLongitude(),6)), getDistance(), docId, locationId);
                } catch (CouchbaseLiteException e) {
                    e.printStackTrace();
                }
                Location.distanceBetween(roundToDecimals(getPrevLat(),6), roundToDecimals(getPrevLng(),6), roundToDecimals(location.getLatitude(),6), roundToDecimals(location.getLongitude(),6), result);

                // Static value 12 is used to restrict the frequent changes of GPS.
                if (result[0] > 12) {
                    distance = getDistance() + result[0];
                    setDistance(distance);
                    session.setDistance(distance);
                    locationDetails = new com.traconmobi.tom.model.Location(location.getLatitude(), location.getLongitude(),
                            distance, locUpdateTime);
                    locationListDetails.add(locationDetails);
                    tripwiseWaypoint.put(tripKey + tripCount, locationListDetails);
                    couchDb.setWaypoint(tripwiseWaypoint, userId, userName, companyId);


                    userTravelDetails = new UserTravelDetails(timezone, transportUsed, batterLevel, distance, String.valueOf(tripCount),
                            locationDetails, "NA", locationId, startTripLocation, startTripTime);
                    userTravelDetailsMap.put(tripKey + tripCount, userTravelDetails);
                    couchDb.setUserTrackHistory(companyId, userId, userName, userTravelDetailsMap);
                }
                setPrevLat(location.getLatitude());
                setPrevLng(location.getLongitude());
            }
        }


        public void onStatusChanged(String s, int i, Bundle b) {

        }

        public void onProviderDisabled(String s) {

        }

        public void onProviderEnabled(String s) {

        }

        public double getPrevLat() {
            return prevLat;
        }

        public double getPrevLng() {
            return prevLng;
        }

        public void setPrevLat(double prevLat) {
            this.prevLat = prevLat;
        }

        public void setPrevLng(double prevLng) {
            this.prevLng = prevLng;
        }

        public void setDistance(float distance) {
            this.distance = distance;
        }

        public float getDistance() {
            return distance;
        }
    }

    protected void showCurrentLocation() {

        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (location != null) {
            String message = String.format(
                    "Current Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );
            Log.e(TAG, message);
        }
    }

    public double roundToDecimals(double d, int c) {
        int temp = (int) (d * Math.pow(10, c));
        return ((double) temp) / Math.pow(10, c);
    }
}
