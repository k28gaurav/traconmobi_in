package com.traconmobi.tom;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;

import android.provider.BaseColumns;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.couchbase.lite.CouchbaseLiteException;
import com.crashlytics.android.Crashlytics;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import io.fabric.sdk.android.Fabric;

public class ParsePickupOutscanXML {

    private static ArrayList<HashMap<String, String>> parsedOutscanList;

    private final String startLists = "arrAWBStatus";

    public static String xmlFile = "";
    private String TAG = "ParsePickupOutscanXML";
    Cursor c_show_assigned_cnt;
    public static int assigned_count, size;
    Set<String> docs;
    public static String u_status;
    public CouchBaseDBHelper dbHelper;
    List<String> savedList = new ArrayList<String>();
    SharedPreferences.Editor editor;
    public static final String PREF_NAME = "CouchPref";
    public String session_DB_PATH, session_DB_PWD, session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME;
    // variable to hold context
    public Context context;
    public List<String> pickUpData;
    public List<String> uidData;
    public Map<String, Object> couchData;
    SharedPreferences pref;
    Set<String> pickupId;
    String uid;
    String saveDocId = "";
    Set<String> pickupDoc;
    public List<String> docsId;
    SessionManager session;
    String pickUpId = "";
    String assignId = "";
    String companyId = "";
    String name = "";
    boolean flag = false;

    public ParsePickupOutscanXML(String xml, Context ctx) {
        try {
            context = ctx;
            //Intializing Fabric
            Fabric.with(ctx, new Crashlytics());
            docsId = new ArrayList<String>();
            pickUpData = new ArrayList<String>();
            xmlFile = xml;
            //context=ctx;
            couchData = new HashMap<String, Object>();
            session = new SessionManager(context);
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception ParseOutscanXmlResponse" + e.getMessage());
        }
    }


    public void parseXml(String xml, Context ctx)// throws Exception
    {
        try {
            Crashlytics.log(android.util.Log.ERROR, TAG, " Start Parsing the data");
            Document document = loadXMLFromString(xmlFile);
            if (document != null) {
                document.getDocumentElement().normalize();
                Element root = document.getDocumentElement();

                Crashlytics.log(android.util.Log.ERROR, TAG, "Root:" + root.getNodeName());

                NodeList nList = document.getElementsByTagName("ASSIGNMENT");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node node = nList.item(temp);
                    Crashlytics.log(android.util.Log.ERROR, TAG, "");
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        //Print each employee's detail
                        Element eElement = (Element) node;
                        couchData.put("ID", eElement.getElementsByTagName("ID").item(0).getTextContent());
                        // Crashlytics.log(android.util.Log.ERROR,TAG,"ASSIGN_ID : " + eElement.getElementsByTagName("ASSIGN_ID").item(0).getTextContent());
                        couchData.put("ASSIGNMENTID", eElement.getElementsByTagName("ASSIGNMENTID").item(0).getTextContent());
                        //uid.add(eElement.getElementsByTagName("UID").item(0).getTextContent());
                        Crashlytics.log(android.util.Log.ERROR, TAG, " ASSIGNMENTID: " + eElement.getElementsByTagName("ASSIGNMENTID").item(0).getTextContent());
                        couchData.put("ASSIGNMENTNO", eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent());
                        //pickUpData.add(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent() + ":" + eElement.getElementsByTagName("ASSIGNMENTID").item(0).getTextContent());
                        //  pickupDoc.add(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent() + ":" + eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"ASSIGNMENT_NO : " + eElement.getElementsByTagName("ASSIGNMENT_NO").item(0).getTextContent());
                        couchData.put("ASSIGNMENTTYPE", eElement.getElementsByTagName("ASSIGNMENTTYPE").item(0).getTextContent());
                        Crashlytics.log(android.util.Log.ERROR, TAG, "ASSIGNMENTTYPE: " + eElement.getElementsByTagName("ASSIGNMENTTYPE").item(0).getTextContent());
                        couchData.put("SHIPPERACCNO", eElement.getElementsByTagName("SHIPPERACCNO").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"NoOfPcs : " + eElement.getElementsByTagName("NoOfPcs").item(0).getTextContent());
                        couchData.put("SHIPPERNAME", eElement.getElementsByTagName("SHIPPERNAME").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"PICKUPTIME : " + eElement.getElementsByTagName("PICKUPTIME").item(0).getTextContent());
                        couchData.put("SHIPPERADDRESS1", eElement.getElementsByTagName("SHIPPERADDRESS1").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"CONSIGNEE_NAME : " + eElement.getElementsByTagName("CONSIGNEE_NAME").item(0).getTextContent());
                        couchData.put("SHIPPERADDRESS2", eElement.getElementsByTagName("SHIPPERADDRESS2").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"ADDR_LINE1 : " + eElement.getElementsByTagName("ADDR_LINE1").item(0).getTextContent());
                        couchData.put("SHIPPERCITY", eElement.getElementsByTagName("SHIPPERCITY").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"ADDR_LINE2 : " + eElement.getElementsByTagName("ADDR_LINE2").item(0).getTextContent());
                        couchData.put("SHIPPERPINCODE", eElement.getElementsByTagName("SHIPPERPINCODE").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"CITY : " + eElement.getElementsByTagName("CITY").item(0).getTextContent());
                        couchData.put("SHIPPEREMAIL", eElement.getElementsByTagName("SHIPPEREMAIL").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"PINCODE : " + eElement.getElementsByTagName("PINCODE").item(0).getTextContent());
                        couchData.put("SHIPPERCONTACTNO", eElement.getElementsByTagName("SHIPPERCONTACTNO").item(0).getTextContent());
                        // Crashlytics.log(android.util.Log.ERROR,TAG,"CONSIGNEE_EMAIL : " + eElement.getElementsByTagName("CONSIGNEE_EMAIL").item(0).getTextContent());
                        couchData.put("CONSIGNEEACCNO", eElement.getElementsByTagName("CONSIGNEEACCNO").item(0).getTextContent());
                        // Crashlytics.log(android.util.Log.ERROR,TAG,"CONSIGNEE_NO : " + eElement.getElementsByTagName("CONSIGNEE_CONTACT_NO").item(0).getTextContent());
                        couchData.put("CONSIGNEENAME", eElement.getElementsByTagName("CONSIGNEENAME").item(0).getTextContent());
                        // Crashlytics.log(android.util.Log.ERROR,TAG,"Location : " + eElement.getElementsByTagName("isCOD").item(0).getTextContent());
                        couchData.put("CONSIGNEEADDRESS1", eElement.getElementsByTagName("CONSIGNEEADDRESS1").item(0).getTextContent());
                        // Crashlytics.log(android.util.Log.ERROR,TAG,"AMOUNT : " + eElement.getElementsByTagName("AMOUNT").item(0).getTextContent());
                        couchData.put("CONSIGNEEADDRESS2", eElement.getElementsByTagName("CONSIGNEEADDRESS2").item(0).getTextContent());
                        // Crashlytics.log(android.util.Log.ERROR,TAG,"Location : " + eElement.getElementsByTagName("ASSIGN_OS_DT").item(0).getTextContent());
                        couchData.put("CONSIGNEECITY", eElement.getElementsByTagName("CONSIGNEECITY").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"ASSIGN_OS_LOC : " + eElement.getElementsByTagName("ASSIGN_OS_LOC").item(0).getTextContent());
                        couchData.put("CONSIGNEEPINCODE", eElement.getElementsByTagName("CONSIGNEEPINCODE").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"ASSIGN_OS_UID : " + eElement.getElementsByTagName("ASSIGN_OS_UID").item(0).getTextContent());
                        couchData.put("CONSIGNEEEMAIL", eElement.getElementsByTagName("CONSIGNEEEMAIL").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"LATITUDE : " + eElement.getElementsByTagName("LATITUDE").item(0).getTextContent());
                        couchData.put("CONSIGNEECONTACTNO", eElement.getElementsByTagName("CONSIGNEECONTACTNO").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"LONGITUDE : " + eElement.getElementsByTagName("LONGITUDE").item(0).getTextContent());
                        couchData.put("ISCOLLECTABLE", eElement.getElementsByTagName("ISCOLLECTABLE").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"SHIPPER_ACC_NO : " + eElement.getElementsByTagName("SHIPPER_ACC_NO").item(0).getTextContent());
                        couchData.put("COLLECTABLEAMOUNT", eElement.getElementsByTagName("COLLECTABLEAMOUNT").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"SHIPPER_CONTACT_NO: " + eElement.getElementsByTagName("SHIPPER_CONTACT_NO").item(0).getTextContent());
                        couchData.put("ASSIGNEDLOCID", eElement.getElementsByTagName("ASSIGNEDLOCID").item(0).getTextContent());
                       // session.setLocId(eElement.getElementsByTagName("ASSIGNEDLOCID").item(0).getTextContent(),);
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"SHIPPER_NAME : " + eElement.getElementsByTagName("SHIPPER_NAME").item(0).getTextContent());
                        couchData.put("ASSIGNEDUSERID", eElement.getElementsByTagName("ASSIGNEDUSERID").item(0).getTextContent());
                        //session.setUserId(eElement.getElementsByTagName("ASSIGNEDUSERID").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"EMAIL : " + eElement.getElementsByTagName("SHIPPER_EMAIL").item(0).getTextContent());
                        couchData.put("ASSIGNEDDATETIME", eElement.getElementsByTagName("ASSIGNEDDATETIME").item(0).getTextContent());
                        //Crashlytics.log(android.util.Log.ERROR,TAG,"NCUST_ACC_NO : " + eElement.getElementsByTagName("NCUST_ACC_NO").item(0).getTextContent());
                        couchData.put("NOOFPIECES", eElement.getElementsByTagName("NOOFPIECES").item(0).getTextContent());
                        couchData.put("EXPECTEDDELIVERY_DATETIME", eElement.getElementsByTagName("EXPECTEDDELIVERY_DATETIME").item(0).getTextContent());
                        couchData.put("MODEOFPAYMENT", eElement.getElementsByTagName("MODEOFPAYMENT").item(0).getTextContent());
                        couchData.put("LATITUDE", eElement.getElementsByTagName("LATITUDE").item(0).getTextContent());
                        couchData.put("LONGITUDE", eElement.getElementsByTagName("LONGITUDE").item(0).getTextContent());
                        couchData.put("COMPANYID", eElement.getElementsByTagName("COMPANYID").item(0).getTextContent());
                        couchData.put("NAME", eElement.getElementsByTagName("NAME").item(0).getTextContent());
                        couchData.put("WAYBILLNO", eElement.getElementsByTagName("WAYBILLNO").item(0).getTextContent());
                        couchData.put("ITEMDESC", eElement.getElementsByTagName("ITEMDESC").item(0).getTextContent());
                        couchData.put("REASONOFRETURN", eElement.getElementsByTagName("REASONOFRETURN").item(0).getTextContent());
                        couchData.put("DECLAREDVALUE", eElement.getElementsByTagName("DECLAREDVALUE").item(0).getTextContent());
                        couchData.put("PCKTIME", eElement.getElementsByTagName("PCKTIME").item(0).getTextContent());
                        couchData.put("ORDERNO", eElement.getElementsByTagName("ORDERNO").item(0).getTextContent());

                        if (couchData.get("ASSIGNMENTTYPE").toString().equals("P") || couchData.get("ASSIGNMENTTYPE").toString().equals("RVP") || couchData.get("ASSIGNMENTTYPE").toString().equalsIgnoreCase("O")) {
                            pickUpId = couchData.get("ASSIGNMENTNO").toString();
                            assignId = couchData.get("ASSIGNMENTID").toString();
                            companyId = couchData.get("COMPANYID").toString();
                            name = session.getUserDetails().get(session.KEY_USER_NAME);
                            Date curDate = new Date();
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                            String datePickUp = dateFormat.format(curDate).toString();
                            if (dbHelper == null) {
                                dbHelper = new CouchBaseDBHelper(context);
                            }
                            if (!dbHelper.queryDocId(pickUpId + "_" + assignId + "_" + companyId + "_" + datePickUp)) {
                                    pickUpData.add(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent() + ":::" +
                                        eElement.getElementsByTagName("ASSIGNMENTID").item(0).getTextContent() + ":::" +
                                        eElement.getElementsByTagName("SHIPPERNAME").item(0).getTextContent() + ":::" +
                                        eElement.getElementsByTagName("SHIPPERADDRESS1").item(0).getTextContent()
                                        + ":::" + eElement.getElementsByTagName("SHIPPERADDRESS2").item(0).getTextContent() + ":::" +
                                        eElement.getElementsByTagName("SHIPPERCONTACTNO").item(0).getTextContent() + ":::" +
                                        eElement.getElementsByTagName("EXPECTEDDELIVERY_DATETIME").item(0).getTextContent() + ":::" +
                                        eElement.getElementsByTagName("ASSIGNEDDATETIME").item(0).getTextContent() + ":::" +
                                        eElement.getElementsByTagName("COMPANYID").item(0).getTextContent() + ":::" +
                                        eElement.getElementsByTagName("NOOFPIECES").item(0).getTextContent());

                                session.setNoPcs(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("NOOFPIECES").item(0).getTextContent());
                                session.setAssignType(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("ASSIGNMENTTYPE").item(0).getTextContent());
                                session.setAssignmentid(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("ASSIGNMENTID").item(0).getTextContent());
                                session.setWayBillNo(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("WAYBILLNO").item(0).getTextContent());
                                session.setItemDescription(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("ITEMDESC").item(0).getTextContent());
                                session.setReasonOfReturn(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("REASONOFRETURN").item(0).getTextContent());
                                session.setDescribedValue(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("DECLAREDVALUE").item(0).getTextContent());
                                session.setShipperPin(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("SHIPPERPINCODE").item(0).getTextContent());
                                session.setShipperCity(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("SHIPPERCITY").item(0).getTextContent());
                                session.setPickTime(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("PCKTIME").item(0).getTextContent());
                                session.setAddress1(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("SHIPPERADDRESS1").item(0).getTextContent());
                                session.setAddress2(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("SHIPPERADDRESS2").item(0).getTextContent());
                                session.setConsigneeName(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("SHIPPERNAME").item(0).getTextContent());
                                session.setOrderNo(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent(), eElement.getElementsByTagName("ORDERNO").item(0).getTextContent());

                                docsId.add(eElement.getElementsByTagName("ASSIGNMENTNO").item(0).getTextContent() +
                                        "_" + eElement.getElementsByTagName("ASSIGNMENTID").item(0).getTextContent() +
                                        "_" + eElement.getElementsByTagName("COMPANYID").item(0).getTextContent() +
                                        "_" + datePickUp);

                                pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
                                editor = pref.edit();
                                editor.clear();
                                editor.putString("compnayId", companyId);
                                editor.putString("name", name);
                                editor.commit();
                                dbHelper.addNewRecord(couchData, pickUpId, datePickUp, companyId);
                                if (eElement.getElementsByTagName("ASSIGNMENTTYPE").item(0).getTextContent().equals("RVP")) {
                                    dbHelper.addPickUpscanRVPRecord(couchData.get("ASSIGNMENTNO").toString(), couchData.get("ASSIGNEDLOCID").toString(), couchData.get("ASSIGNEDUSERID").toString(), couchData.get("COMPANYID").toString(), couchData.get("ASSIGNMENTID").toString(), datePickUp, eElement.getElementsByTagName("WAYBILLNO").item(0).getTextContent(),
                                            eElement.getElementsByTagName("ITEMDESC").item(0).getTextContent(), eElement.getElementsByTagName("REASONOFRETURN").item(0).getTextContent(), eElement.getElementsByTagName("DECLAREDVALUE").item(0).getTextContent(), eElement.getElementsByTagName("NOOFPIECES").item(0).getTextContent());

                                } else if (eElement.getElementsByTagName("ASSIGNMENTTYPE").item(0).getTextContent().equalsIgnoreCase("O")) {
                                    dbHelper.addECOMscanRecord(couchData.get("ASSIGNMENTNO").toString(), couchData.get("ASSIGNEDLOCID").toString(), couchData.get("ASSIGNEDUSERID").toString(), couchData.get("COMPANYID").toString(), couchData.get("ASSIGNMENTID").toString(), datePickUp);
                                } else {
                                    dbHelper.addPickUpscanRecord(couchData.get("ASSIGNMENTNO").toString(), couchData.get("ASSIGNEDLOCID").toString(), couchData.get("ASSIGNEDUSERID").toString(), couchData.get("COMPANYID").toString(), couchData.get("ASSIGNMENTID").toString(), datePickUp);
                                }
                                //dbHelper.addPickUpscanRecord(couchData.get("ASSIGNMENTNO").toString(), couchData.get("ASSIGNEDLOCID").toString(), couchData.get("ASSIGNEDUSERID").toString(), couchData.get("COMPANYID").toString(), couchData.get("ASSIGNMENTID").toString(), datePickUp);
                            }
                        }
                    }
                }

                try {
                    if (pickUpData != null) {
                        session = new SessionManager(context);
                        if (pickUpData.size() > 0) {
                            Set<String> setPickUp = session.getPickUpId(name);
                            Set<String> setDocs = session.getDocs(name);

                            if (setPickUp != null) {
                                String[] dataId = setPickUp.toArray(new String[setPickUp.size()]);
                                //String[] docId = null;
                                if (setDocs != null) {
                                    String[] docId = setDocs.toArray(new String[setDocs.size()]);
                                    setDocs.addAll(Arrays.asList(docId));
                                    setDocs.addAll(docsId);
                                    session.setDocs(name, setDocs);
                                }
                                savedList = Arrays.asList(dataId);
                                setPickUp.addAll(savedList);
                                setPickUp.addAll(pickUpData);
                                session.setPickUpId(name, setPickUp);
                            } else {
                                docs = new HashSet<>(docsId);
                                pickupId = new HashSet<>(pickUpData);
                                session.setDocs(name, docs);
                                session.setPickUpId(name, pickupId);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Crashlytics.log(Log.ERROR, TAG, "Exception in pickupscan List of parseOutScan XML " + e.getMessage());
                    Crashlytics.logException(e);
                }
            }
        } catch (Exception err) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "parseXml" + err.getMessage());
            Crashlytics.logException(err);
        }
    }

    public Document loadXMLFromString(String xml) {
        Crashlytics.log(android.util.Log.ERROR, TAG, "Load Xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource is = new InputSource(new StringReader(xml));
        try {
            return builder.parse(is);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
