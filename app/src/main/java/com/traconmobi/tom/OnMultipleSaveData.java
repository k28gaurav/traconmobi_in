package com.traconmobi.tom;

/**
 * Created by kumargaurav on 6/30/16.
 */
public interface OnMultipleSaveData {
    public void onSave();
}
