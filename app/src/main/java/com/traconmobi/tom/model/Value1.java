package com.traconmobi.tom.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kumargaurav on 1/30/16.
 */
public class Value1 {

    @SerializedName("AssignmentNo")

    private String AssignmentNo;

    @SerializedName("CompanyId")
    private String CompanyId;

    @SerializedName("LocID")
    private String LocID;

    @SerializedName("UID")
    private String UID;

    @SerializedName("completeStatus")
    private String completeStatus;

    @SerializedName("created_on")
    private String createdOn;

    @SerializedName("Latitude")
    private Double Latitude;

    @SerializedName("Longitude")
    private Double Longitude;

    @SerializedName("waybillNo")
    private String waybillNo;

    @SerializedName("itemDescription")
    private String itemDescription;

    @SerializedName("pickupReason")
    private String pickupReason;

    @SerializedName("pickupRemark")
    private String pickupRemark;

    @SerializedName("custName")
    private String custName;

    @SerializedName("custNo")
    private String custNo;

    @SerializedName("relationship")
    private String relationship;

    @SerializedName("returnReason")
    private String returnReason;

    @SerializedName("describedValue")
    private String describedValue;

    @SerializedName("image")
    private String image;

    @SerializedName("signature")
    private String signature;

    /**
     *
     * @return
     * The AssignmentNo
     */
    public String getAssignmentNo() {
        return AssignmentNo;
    }

    /**
     *
     * @param AssignmentNo
     * The AssignmentNo
     */
    public void setAssignmentNo(String AssignmentNo) {
        this.AssignmentNo = AssignmentNo;
    }

    /**
     *
     * @return
     * The CompanyId
     */
    public String getCompanyId() {
        return CompanyId;
    }

    /**
     *
     * @param CompanyId
     * The CompanyId
     */
    public void setCompanyId(String CompanyId) {
        this.CompanyId = CompanyId;
    }

    /**
     *
     * @return
     * The LocID
     */
    public String getLocID() {
        return LocID;
    }

    /**
     *
     * @param LocID
     * The LocID
     */
    public void setLocID(String LocID) {
        this.LocID = LocID;
    }

    /**
     *
     * @return
     * The UID
     */
    public String getUID() {
        return UID;
    }

    /**
     *
     * @param UID
     * The UID
     */
    public void setUID(String UID) {
        this.UID = UID;
    }

    /**
     *
     * @return
     * The completeStatus
     */
    public String getCompleteStatus() {
        return completeStatus;
    }

    /**
     *
     * @param completeStatus
     * The completeStatus
     */
    public void setCompleteStatus(String completeStatus) {
        this.completeStatus = completeStatus;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @param createdOn
     * The created_on
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     *
     * @return
     * The Latitude
     */
  /*  public Integer getLatitude() {
        return Latitude;
    }

    *//**
     *
     * @param Latitude
     * The Latitude
     *//*
    public void setLatitude(Integer Latitude) {
        this.Latitude = Latitude;
    }

    *//**
     *
     * @return
     * The Longitude
     *//*
    public Integer getLongitude() {
        return Longitude;
    }

    *//**
     *
     * @param Longitude
     * The Longitude
     *//*
    public void setLongitude(Integer Longitude) {
        this.Longitude = Longitude;
    }*/

    /**
     *
     * @return
     * The waybillNo
     */
    public String getWaybillNo() {
        return waybillNo;
    }

    /**
     *
     * @param waybillNo
     * The waybillNo
     */
    public void setWaybillNo(String waybillNo) {
        this.waybillNo = waybillNo;
    }

    /**
     *
     * @return
     * The itemDescription
     */
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     *
     * @param itemDescription
     * The itemDescription
     */
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    /**
     *
     * @return
     * The pickupReason
     */
    public String getPickupReason() {
        return pickupReason;
    }

    /**
     *
     * @param pickupReason
     * The pickupReason
     */
    public void setPickupReason(String pickupReason) {
        this.pickupReason = pickupReason;
    }

    /**
     *
     * @return
     * The pickupRemark
     */
    public String getPickupRemark() {
        return pickupRemark;
    }

    /**
     *
     * @param pickupRemark
     * The pickupRemark
     */
    public void setPickupRemark(String pickupRemark) {
        this.pickupRemark = pickupRemark;
    }

    /**
     *
     * @return
     * The custName
     */
    public String getCustName() {
        return custName;
    }

    /**
     *
     * @param custName
     * The custName
     */
    public void setCustName(String custName) {
        this.custName = custName;
    }

    /**
     *
     * @return
     * The custNo
     */
    public String getCustNo() {
        return custNo;
    }

    /**
     *
     * @param custNo
     * The custNo
     */
    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    /**
     *
     * @return
     * The relationship
     */
    public String getRelationship() {
        return relationship;
    }

    /**
     *
     * @param relationship
     * The relationship
     */
    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    /**
     *
     * @return
     * The returnReason
     */
    public String getReturnReason() {
        return returnReason;
    }

    /**
     *
     * @param returnReason
     * The returnReason
     */
    public void setReturnReason(String returnReason) {
        this.returnReason = returnReason;
    }

    /**
     *
     * @return
     * The describedValue
     */
    public String getDescribedValue() {
        return describedValue;
    }

    /**
     *
     * @param describedValue
     * The describedValue
     */
    public void setDescribedValue(String describedValue) {
        this.describedValue = describedValue;
    }

    /**
     *
     * @return
     * The image
     */
    public String getImage() {
        return image;
    }

    /**
     *
     * @param image
     * The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     *
     * @return
     * The signature
     */
    public String getSignature() {
        return signature;
    }

    /**
     *
     * @param signature
     * The signature
     */
    public void setSignature(String signature) {
        this.signature = signature;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }
}

