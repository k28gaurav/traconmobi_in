package com.traconmobi.tom.model;

/**
 * Created by kumargaurav on 7/29/16.
 */
public class TrackAllUsers {
    private String userName;
    private String userId;
    private String companyId;
    private double distance;
    private double lat;
    private double lng;
    private String locid;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getLocid() {
        return locid;
    }

    public void setLocid(String locid) {
        this.locid = locid;
    }

    public TrackAllUsers(String userName, String userId, String companyId, double distance, double lat, double lng, String locid) {

        this.userName = userName;
        this.userId = userId;
        this.companyId = companyId;
        this.distance = distance;
        this.lat = lat;
        this.lng = lng;
        this.locid = locid;
    }
    public TrackAllUsers(){

    }
}
