package com.traconmobi.tom.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kumargaurav on 2/9/16.
 */
public class Value2 {

    @SerializedName("AssignmentNo")

    private String AssignmentNo;
    @SerializedName("CompanyId")

    private String CompanyId;
    @SerializedName("LocID")

    private String LocID;
    @SerializedName("UID")

    private String UID;
    @SerializedName("completeStatus")

    private String completeStatus;
    @SerializedName("created_on")

    private String createdOn;
    @SerializedName("Latitude")

/*    private Integer Latitude;
    @SerializedName("Longitude")

    private Integer Longitude;
    @SerializedName("pickupReason")*/

    private String pickupReason;

    @SerializedName("reasonId")
    private String reasonId;

    @SerializedName("pickupRemark")

    private String pickupRemark;
    @SerializedName("scanItem")

    private List<String> scanItem = new ArrayList<String>();
    @SerializedName("doctype")

    private String doctype;

    /**
     *
     * @return
     * The AssignmentNo
     */
    public String getAssignmentNo() {
        return AssignmentNo;
    }

    /**
     *
     * @param AssignmentNo
     * The AssignmentNo
     */
    public void setAssignmentNo(String AssignmentNo) {
        this.AssignmentNo = AssignmentNo;
    }

    /**
     *
     * @return
     * The CompanyId
     */
    public String getCompanyId() {
        return CompanyId;
    }

    /**
     *
     * @param CompanyId
     * The CompanyId
     */
    public void setCompanyId(String CompanyId) {
        this.CompanyId = CompanyId;
    }

    /**
     *
     * @return
     * The LocID
     */
    public String getLocID() {
        return LocID;
    }

    /**
     *
     * @param LocID
     * The LocID
     */
    public void setLocID(String LocID) {
        this.LocID = LocID;
    }

    /**
     *
     * @return
     * The UID
     */
    public String getUID() {
        return UID;
    }

    /**
     *
     * @param UID
     * The UID
     */
    public void setUID(String UID) {
        this.UID = UID;
    }

    /**
     *
     * @return
     * The completeStatus
     */
    public String getCompleteStatus() {
        return completeStatus;
    }

    /**
     *
     * @param completeStatus
     * The completeStatus
     */
    public void setCompleteStatus(String completeStatus) {
        this.completeStatus = completeStatus;
    }

    /**
     *
     * @return
     * The createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     *
     * @param createdOn
     * The created_on
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     *
     * @return
     * The Latitude
     */
  /*  public Integer getLatitude() {
        return Latitude;
    }

    *//**
     *
     * @param Latitude
     * The Latitude
     *//*
    public void setLatitude(Integer Latitude) {
        this.Latitude = Latitude;
    }

    *//**
     *
     * @return
     * The Longitude
     *//*
    public Integer getLongitude() {
        return Longitude;
    }

    *//**
     *
     * @param Longitude
     * The Longitude
     *//*
    public void setLongitude(Integer Longitude) {
        this.Longitude = Longitude;
    }*/

    /**
     *
     * @return
     * The pickupReason
     */
    public String getPickupReason() {
        return pickupReason;
    }

    /**
     *
     * @param pickupReason
     * The pickupReason
     */
    public void setPickupReason(String pickupReason) {
        this.pickupReason = pickupReason;
    }

    /**
     *
     * @return
     * The pickupRemark
     */
    public String getPickupRemark() {
        return pickupRemark;
    }

    /**
     *
     * @param pickupRemark
     * The pickupRemark
     */
    public void setPickupRemark(String pickupRemark) {
        this.pickupRemark = pickupRemark;
    }

    /**
     *
     * @return
     * The scanItem
     */
    public List<String> getScanItem() {
        return scanItem;
    }

    /**
     *
     * @param scanItem
     * The scanItem
     */
    public void setScanItem(List<String> scanItem) {
        this.scanItem = scanItem;
    }

    /**
     *
     * @return
     * The doctype
     */
    public String getDoctype() {
        return doctype;
    }

    /**
     *
     * @param doctype
     * The doctype
     */
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getReasonid() {
        return reasonId;
    }

    /**
     *
     * @param reasonId
     * The pickupReason
     */
    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }
}
