package com.traconmobi.tom.model;

/**
 * Created by kumargaurav on 12/22/15.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Row1 {

    @SerializedName("id")
    private String id;

    @SerializedName("key")
    private String key;

    @SerializedName("value")
    private Value1 value;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The key
     */
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key
     * The key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @return
     * The value
     */
    public Value1 getValue() {
        return value;
    }

    /**
     *
     * @param value
     * The value
     */
    public void setValue(Value1 value) {
        this.value = value;
    }

}
