package com.traconmobi.tom.model;

/**
 * Created by hp on 30/7/16.
 */

public class Location {
    private double lat;
    private double lng;
    private float distance;
    private String time;


    public Location(double lat, double lng, float distance, String time) {
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
        this.time = time;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}