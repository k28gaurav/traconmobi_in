package com.traconmobi.tom.model;

/**
 * Created by hp on 30/7/16.
 */

public class UserTravelDetails {
    private String timezone;
    private int battery_life;
    private float distance_travelled;
    private String trip_count;
    private Location location;
    private String endLocation;
    private String locationid;
    private String startLocation;
    private String transport_time;
    private String transport_used;

    public UserTravelDetails(String timezone, String transport_used, int battery_life,
                             float distance_travelled, String trip_count, Location location,
                             String endLocation, String locationid, String startLocation, String transport_time) {
        this.timezone = timezone;
        this.transport_used = transport_used;
        this.battery_life = battery_life;
        this.distance_travelled = distance_travelled;
        this.trip_count = trip_count;
        this.location = location;
        this.endLocation = endLocation;
        this.locationid = locationid;
        this.startLocation = startLocation;
        this.transport_time = transport_time;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public int getBattery_life() {
        return battery_life;
    }

    public void setBattery_life(int battery_life) {
        this.battery_life = battery_life;
    }

    public float getDistance_travelled() {
        return distance_travelled;
    }

    public void setDistance_travelled(float distance_travelled) {
        this.distance_travelled = distance_travelled;
    }

    public String getTrip_count() {
        return trip_count;
    }

    public void setTrip_count(String trip_count) {
        this.trip_count = trip_count;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public String getLocationid() {
        return locationid;
    }

    public void setLocationid(String locationid) {
        this.locationid = locationid;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getTransport_time() {
        return transport_time;
    }

    public void setTransport_time(String transport_time) {
        this.transport_time = transport_time;
    }

    public String getTransport_used() {
        return transport_used;
    }

    public void setTransport_used(String transport_used) {
        this.transport_used = transport_used;
    }
}