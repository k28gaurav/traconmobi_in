package com.traconmobi.tom.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kumargaurav on 2/6/16.
 */
public class Key2 {
    @SerializedName("ASSIGNMENTID")
    private Integer ASSIGNMENTID;

    @SerializedName("ASSIGNMENTNO")
    private String ASSIGNMENTNO;

    @SerializedName("ASSIGNMENT_TYPE")
    private String ASSIGNMENTTYPE;

    @SerializedName("SCAN_ITEM")
    private List<String> SCANITEM = new ArrayList<String>();

    @SerializedName("LOCATION")
    private Integer LOCATION;

    @SerializedName("USER_ID")
    private Integer USERID;

    @SerializedName("COMPANY_ID")
    private Integer COMPANYID;

    @SerializedName("ORDER_DATE")
    private String ORDERDATE;

    @SerializedName("DOC_TYPE")
    private String DOCTYPE;

    /**
     *
     * @return
     * The ASSIGNMENTID
     */
    public Integer getASSIGNMENTID() {
        return ASSIGNMENTID;
    }

    /**
     *
     * @param ASSIGNMENTID
     * The ASSIGNMENTID
     */
    public void setASSIGNMENTID(Integer ASSIGNMENTID) {
        this.ASSIGNMENTID = ASSIGNMENTID;
    }

    /**
     *
     * @return
     * The ASSIGNMENTNO
     */
    public String getASSIGNMENTNO() {
        return ASSIGNMENTNO;
    }

    /**
     *
     * @param ASSIGNMENTNO
     * The ASSIGNMENTNO
     */
    public void setASSIGNMENTNO(String ASSIGNMENTNO) {
        this.ASSIGNMENTNO = ASSIGNMENTNO;
    }

    /**
     *
     * @return
     * The ASSIGNMENTTYPE
     */
    public String getASSIGNMENTTYPE() {
        return ASSIGNMENTTYPE;
    }

    /**
     *
     * @param ASSIGNMENTTYPE
     * The ASSIGNMENT_TYPE
     */
    public void setASSIGNMENTTYPE(String ASSIGNMENTTYPE) {
        this.ASSIGNMENTTYPE = ASSIGNMENTTYPE;
    }

    public List<String> getSCANITEM() {
        return SCANITEM;
    }

    /**
     *
     * @param SCANITEM
     * The SCAN_ITEM
     */
    public void setSCANITEM(List<String> SCANITEM) {
        this.SCANITEM = SCANITEM;
    }

    /**
     *
     * @return
     * The LOCATION
     */
    public Integer getLOCATION() {
        return LOCATION;
    }

    /**
     *
     * @param LOCATION
     * The LOCATION
     */
    public void setLOCATION(Integer LOCATION) {
        this.LOCATION = LOCATION;
    }

    /**
     *
     * @return
     * The USERID
     */
    public Integer getUSERID() {
        return USERID;
    }

    /**
     *
     * @param USERID
     * The USER_ID
     */
    public void setUSERID(Integer USERID) {
        this.USERID = USERID;
    }

    /**
     *
     * @return
     * The COMPANYID
     */
    public Integer getCOMPANYID() {
        return COMPANYID;
    }

    /**
     *
     * @param COMPANYID
     * The COMPANY_ID
     */
    public void setCOMPANYID(Integer COMPANYID) {
        this.COMPANYID = COMPANYID;
    }

    /**
     *
     * @return
     * The ORDERDATE
     */
    public String getORDERDATE() {
        return ORDERDATE;
    }

    /**
     *
     * @param ORDERDATE
     * The ORDER_DATE
     */
    public void setORDERDATE(String ORDERDATE) {
        this.ORDERDATE = ORDERDATE;
    }

    public String getDOCTYPE() {
        return DOCTYPE;
    }

    /**
     *
     * @param DOCTYPE
     * The DOC_TYPE
     */
    public void setDOCTYPE(String DOCTYPE) {
        this.DOCTYPE = DOCTYPE;
    }
}
