package com.traconmobi.tom;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.SQLException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;
import android.util.Log;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class ReportsMainActivity extends Activity {

    protected SQLiteDatabase db;
    TextView tt1, tt2, tt3, tt4, tt5, tt6, tt7, tt8, tt9, version_name, km_trvld, calndr_val, mnth_name;
    double total_amt;
    public String str_amt, curnt_dt, awb_outscan_date, flag, flg, fltr_typ;
    int cr_unattemptcount, cr_delcount, cr_undelcount;
    // Session Manager Class
    SessionManager session;
    public String session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE,
            session_USER_NAME, session_DB_PATH, session_DB_PWD;
    public String gps_km, Mnth_nme;
    float distance;
    TextView title;
    public String TAG = "ReportsMainActivity";
    Calendar ca1;
    String date;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.report_dtls);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.report_dtls);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("DAY STATUS");


//	    tt1=(TextView)findViewById(R.id.awb_CELL);
            tt2 = (TextView) findViewById(R.id.ref_CELL);
//        tt3=(TextView)findViewById(R.id.date_CELL);
            tt4 = (TextView) findViewById(R.id.origin_CELL);
//        tt5=(TextView)findViewById(R.id.dest_CELL);
            tt6 = (TextView) findViewById(R.id.status_CELL);
//        tt7=(TextView)findViewById(R.id.reason_CELL);
            tt8 = (TextView) findViewById(R.id.recvby_CELL);

            km_trvld = (TextView) findViewById(R.id.trvl_val);

            mnth_name = (TextView) findViewById(R.id.cal_txt);
            calndr_val = (TextView) findViewById(R.id.cal_val);

            // Session class instance
            session = new SessionManager(getApplicationContext());
            // session_CUST_ACC_CODE


            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            try {
                ca1 = Calendar.getInstance();
                java.util.Date d = new java.util.Date(ca1.getTimeInMillis());
                Mnth_nme = new SimpleDateFormat("MMM").format(d).toUpperCase().toString().trim();
                mnth_name.setText(Mnth_nme);
            } catch (Exception e) {
                Crashlytics.log(android.util.Log.ERROR, TAG, e.getMessage());
            }
        } catch (Exception e) {
        }

        try {
            if (session_CURRENT_DT != null && session_CURRENT_DT != "") {
//				2015-11-18
                date = session_CURRENT_DT.substring(8, 10);
                try {
                    calndr_val.setText(date);
                } catch (NullPointerException e) {

                }
            }

           /* gps_km = session.getText();

            if (gps_km != null && gps_km != "") {
                String[] mapData = gps_km.split(":");
                if (mapData.length > 2) {

                    double lat = Double.parseDouble(mapData[0]);
                    double lng = Double.parseDouble(mapData[1]);
                    float dis = Float.parseFloat(mapData[2]);
                    try {
                        km_trvld.setText(String.valueOf(roundToDecimals(dis / 1000, 3)) + "Km");
                    } catch (NullPointerException e) {

                    }
                }
            } else {
                try {
                    km_trvld.setText("0" + "KM");
                } catch (NullPointerException e) {

                }
            }*/

           /* new CursorTask().execute();*/

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error ReportsMainActivity Exception ");
//			onClickGoToHomePage();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error ReportsMainActivity UnsatisfiedLinkError ");
// 		onClickGoToHomePage();
        }
    }

    public static double roundToDecimals(double d, int c) {
        int temp = (int) (d * Math.pow(10, c));
        return ((double) temp) / Math.pow(10, c);
    }

    public void toggleMenu(View v) {
        finish();
    }

    public void onclk_noti(View v) {
        Intent homeActivity = new Intent(this, NotificationActivity.class);
        startActivity(homeActivity);
    }

    public void onclk_trip(View v) {
        Intent homeActivity = new Intent(this, Start_End_TripMainActivity.class);
        startActivity(homeActivity);
    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
           /* stopService(new Intent(getApplicationContext(), LocationLoggerService.class));
            stopService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));*/
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception ReportsMainActivity onDestroy " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error ReportsMainActivity UnsatisfiedLinkError ");
        }
    }

    //**********************modification for menu options(Home & Logout)**************************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                onClickGoToHomePage();
                return true;
            case R.id.menu_log_out:
                onClickLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (session == null) {
            session = new SessionManager(getApplicationContext());
        }


        try {
            if (session_CURRENT_DT != null && session_CURRENT_DT != "") {
//				2015-11-18
                date = session_CURRENT_DT.substring(8, 10);
                try {
                    calndr_val.setText(date);
                } catch (NullPointerException e) {

                }
            }

            distance = session.getDistance();
            Log.e(TAG, "Distance travelled: " + distance);
            km_trvld.setText(String.valueOf(round(distance/1000,3)));


            new CursorTask().execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error ReportsMainActivity Exception ");
//			onClickGoToHomePage();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error ReportsMainActivity UnsatisfiedLinkError ");
// 		onClickGoToHomePage();
        }
    }

    private void onClickLogOut() {
        try {
            Intent logOutActivity = new Intent(this, TOMLoginUserActivity.class);
            logOutActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logOutActivity);
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception ReportsMainActivity onClickLogOut " + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "Error ReportsMainActivity UnsatisfiedLinkError ");
        }
    }

    private void onClickGoToHomePage() {
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }


    private class CursorTask extends AsyncTask<Void, Void, Void> {
        final ProgressDialog dialog = new ProgressDialog(ReportsMainActivity.this);


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait for Report Building...");
            this.dialog.show();
            this.dialog.setCancelable(true);
        }


        CursorTask() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                if (session_DB_PATH != null && session_DB_PWD != null) {

                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);

                    flag = "D";
                    Cursor c_outscan_date = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name, emp .T_Address_Line1, " +
                            "emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Completed,emp.T_Assignment_Type," +
                            "emp.D_OutScan_Date FROM TOM_Assignments emp  where emp.T_Assignment_Type='" + flag + "' AND emp.T_OUT_Scan_U_ID='" +
                            session_USER_NUMERIC_ID + "' AND emp.D_OutScan_Date='" + session_CURRENT_DT + "'", null);
                    while (c_outscan_date.moveToNext()) {
                        awb_outscan_date = c_outscan_date.getString(c_outscan_date.getColumnIndex("D_OutScan_Date"));
                    }
                    c_outscan_date.close();

                    /**Code to get the count of unattempted AWBs deliveries for the current date**/
                    if (awb_outscan_date == null) {
                        cr_unattemptcount = 0;
                    } else if (awb_outscan_date != null)
                    //if(awb_outscan_date.equals(session_CURRENT_DT))
                    {
                        flag = "D";
                        fltr_typ = "A";
                        Cursor c = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name, emp .T_Address_Line1, " +
                                "emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Completed," +
                                "emp.T_Assignment_Type FROM TOM_Assignments emp  where emp.B_is_Completed IS NULL AND emp.T_Assignment_Type='"
                                + flag + "' AND emp.T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID + "' AND emp.D_OutScan_Date='" + session_CURRENT_DT + "'", null);
                        //  '%"+ tv.getText().toString().trim() +"%'", null);
                        cr_unattemptcount = c.getCount();
                        c.close();
                    } else {
                        cr_unattemptcount = 0;
                    }


                    /**Code to get the count of delivered AWBs for the current date**/

                    if (awb_outscan_date == null) {
                        cr_delcount = 0;
                    } else if (awb_outscan_date != null)
//        	if(awb_outscan_date.equals(session_CURRENT_DT))
                    {
                        flag = "D";
                        flg = "TRUE";
                        Cursor cr = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name, emp .T_Address_Line1, " +
                                "emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable," +
                                "emp.F_Amount,emp.T_Signature,emp.B_is_Completed,emp.F_Amount_Collected FROM TOM_Assignments emp  " +
                                "where emp.B_is_Completed='" + flg + "' AND emp.T_Assignment_Type='" + flag + "' AND emp.T_OUT_Scan_U_ID='" +
                                session_USER_NUMERIC_ID + "' AND emp.D_OutScan_Date='" + session_CURRENT_DT + "'", null);
                        //  '%"+ tv.getText().toString().trim() +"%'", null);
                        cr_delcount = cr.getCount();
                        cr.close();
                    } else {
                        cr_delcount = 0;
                    }
                    /**Code to get the count of Undelivered AWBs for the current date**/
                    if (awb_outscan_date == null) {
                        cr_undelcount = 0;
                    } else if (awb_outscan_date != null) {
                        flag = "D";
                        fltr_typ = "A";
                        flg = "FALSE";
                        Cursor crs = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name, emp .T_Address_Line1, " +
                                "emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable," +
                                "emp.F_Amount,emp.T_Signature,emp.B_is_Completed,emp.F_Amount_Collected FROM TOM_Assignments emp  " +
                                "where emp.B_is_Completed='" + flg + "' AND emp.T_Assignment_Type='" + flag + "' AND emp.T_OUT_Scan_U_ID='" +
                                session_USER_NUMERIC_ID + "' AND emp.D_OutScan_Date='" + session_CURRENT_DT + "'", null);
                        //  '%"+ tv.getText().toString().trim() +"%'", null);
                        cr_undelcount = crs.getCount();
                        crs.close();
                    } else {
                        cr_undelcount = 0;
                    }
                    /**Code to get the count of F_Amount_Collected AWBs for the current date**/
                    if (awb_outscan_date == null) {
                        str_amt = "00.00";
                    } else if (awb_outscan_date != null)
//        	if(awb_outscan_date.equals(session_CURRENT_DT))
                    {
                        flag = "D";
                        flg = "TRUE";
                        Cursor cursor_amt = db.rawQuery("SELECT SUM(F_Amount_Collected) FROM TOM_Assignments where B_is_Completed='" + flg + "'" +
                                " and T_Assignment_Type='" + flag + "' AND T_OUT_Scan_U_ID='" + session_USER_NUMERIC_ID + "' AND D_OutScan_Date='"
                                + session_CURRENT_DT + "' ", null);

                        while (cursor_amt.moveToNext()) {
                            total_amt = cursor_amt.getDouble(0);
                            NumberFormat formatter = new DecimalFormat("###.00");
                            str_amt = formatter.format(total_amt);
                        }
                        int cnt = cursor_amt.getCount();
                        Crashlytics.log(android.util.Log.ERROR, TAG, "cnt " + cnt);
                        cursor_amt.close();
                    } else {
                        str_amt = "00.00";
                    }
                    db.close();
                } else {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "Error ReportsMainActivity TOMLoginUserActivity.file is null ");
                }
            } catch (OutOfMemoryError e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (SQLException e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                db.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void reult) {
            try {
                tt2.setText(Integer.toString(cr_unattemptcount));
                tt4.setText(Integer.toString(cr_delcount));
                tt6.setText(Integer.toString(cr_undelcount));
                tt8.setText(str_amt);
            }catch(NullPointerException e) {

            }catch(Exception e){

            }
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

        }
    }
}
