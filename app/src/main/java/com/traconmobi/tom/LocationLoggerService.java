package com.traconmobi.tom;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

//import com.google.android.maps.GeoPoint;
import com.crashlytics.android.Crashlytics;
import com.traconmobi.tom.http.OkHttpHandler;
import com.traconmobi.tom.http.OkHttpHandlerPost;
import com.traconmobi.tom.model.UserTravelDetails;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import io.fabric.sdk.android.Fabric;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class LocationLoggerService extends Service {

    // The minimum distance to change Updates in meters
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 60 * 1000; // 1 minute

    private final static String TAG = "LocationLoggerService";
    public String token = "a2lfrq40kdh0nrut1vl0ani4a2";
    public int shw_btry_lvl;
    Location iLoc = null;
    Location mloc;
    String new_lat;
    String new_lon;
    String docId, companyId;
    List<String> latlngList = new ArrayList<String>();
    private static String startTripLocation;
    private static String startTripTime;
    private static String transportUsed;
    private static String endTripLocation;
    boolean flag = false;
    Map<String, List<com.traconmobi.tom.model.Location>> tripwiseWaypoint = new
            HashMap<String, List<com.traconmobi.tom.model.Location>>();

    Map<String, UserTravelDetails> userTravelDetailsMap = new HashMap<String, UserTravelDetails>();
    private  String tripKey = "Trip";
    boolean isRunning = false;
    LocationManager mlocManager;
    LocationListener mlocListener;
    List<com.traconmobi.tom.model.Location> locationListDetails;
    com.traconmobi.tom.model.Location locationDetails;
    Date curDate = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    String docTime = dateFormat.format(curDate).toString();

    //MapSyncDB couchDb;
    CouchBaseDBHelper couchDb = null;
    float distance = 0;
    public String Text;
    float accurcy, spd;
    //  GPXWriter writer;
    @SuppressWarnings("unused")
    private final String URL_gps_log = "http://traconmobi.net/UserTrack-History";
    SQLiteDatabase db;
    String entityString2;
    Cursor cursor;
    String DATE;
    UserTravelDetails userTravelDetails;
    // variable to hold context
    public Context context;

    // Session Manager Class
    SessionManager session;
    public String session_DB_PATH, session_DB_PWD, session_user_id, session_user_pwd, session_USER_LOC,
            session_USER_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME;


    public LocationLoggerService() {
    }

    private void openDb() {
        if (couchDb == null) {
            couchDb = new CouchBaseDBHelper(getApplicationContext());
        }
        if (session == null) {
            session = new SessionManager(getApplicationContext());
        }
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        try {
            Fabric.with(this, new Crashlytics());
             /* Use the LocationManager class to obtain GPS locations */
            Log.e(TAG, "Service onCreate");
            openDb();


            session_USER_NAME = session.getUserDetails().get(session.KEY_USER_NAME);
            session_CUST_ACC_CODE = session.getUserDetails().get(session.KEY_CUST_ACC_CODE);
            session_USER_ID = session.getUserDetails().get(session.KEY_USER_NUMERIC_ID);
            docId = session_USER_NAME + "_" + session_CUST_ACC_CODE;
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(Log.ERROR, TAG, "Exception onCreate" + e.getMessage());
        }
    }

    @Override
    public int onStartCommand(Intent myIntent, int flags, int startId) {

        Log.i(TAG, "Service onStartCommand");
        startTripLocation = myIntent.getStringExtra("startLoc");
        transportUsed = myIntent.getStringExtra("transport_val");
        startTripTime = myIntent.getStringExtra("starttime");
        Log.e(TAG, "Intent data: " + startTripLocation + ":" + transportUsed + ":" + startTripTime);

        if(couchDb.getWaypoint(session_USER_ID, session_CUST_ACC_CODE) != null) {
            tripwiseWaypoint = couchDb.getWaypoint(session_USER_ID, session_CUST_ACC_CODE);
        }

        if(couchDb.getUserTrackHistory(session_USER_ID, session_CUST_ACC_CODE) != null) {
            userTravelDetailsMap = couchDb.getUserTrackHistory(session_USER_ID, session_CUST_ACC_CODE);
            Log.e(TAG, "TripwiseuserHistory: " + userTravelDetailsMap + ":" + userTravelDetailsMap.get("trip2"));
        }

        locationListDetails = new ArrayList<com.traconmobi.tom.model.Location>();
        mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mlocListener = new MyLocationListener();
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                0, mlocListener);

        /*if (mlocManager != null) {
            mloc = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Log.e(TAG, "Initial Location" + mloc);
            if (mloc != null) {
                session.setText(String.valueOf(mloc.getLatitude()) + ":" + String.valueOf(mloc.getLongitude()));
                locationDetails = new com.traconmobi.tom.model.Location(mloc.getLatitude(),mloc.getLongitude(),
                        session.getDistance(), docTime);
                locationListDetails.add(locationDetails);
                tripwiseWaypoint.put(tripKey + session.getEndTripCount(), locationListDetails);
                couchDb.setWaypoint(tripwiseWaypoint,session_USER_ID,session_USER_NAME,session_CUST_ACC_CODE);
            }
            else{
                shw_btry_lvl = PowerConnectionReceiver.level;
                locationDetails = new com.traconmobi.tom.model.Location(0.0,0.0,
                        session.getDistance(), docTime);
                userTravelDetails = new UserTravelDetails(session.getTimezone(), transportUsed, shw_btry_lvl, session.getDistance(),
                        String.valueOf(session.getEndTripCount()),
                        locationDetails,"NA", session.getLocId(), startTripLocation, startTripTime);
                userTravelDetailsMap.put(tripKey + session.getEndTripCount(), userTravelDetails);
                couchDb.setUserTrackHistory(session_CUST_ACC_CODE, session_USER_ID, session_USER_NAME, userTravelDetailsMap);
                locationListDetails.add(locationDetails);
                tripwiseWaypoint.put(tripKey + session.getEndTripCount(), locationListDetails);
                couchDb.setWaypoint(tripwiseWaypoint,session_USER_ID,session_USER_NAME,session_CUST_ACC_CODE);


            }
        }*/
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Service onDestroy");
        session.setDistance(0);
        mlocManager.removeUpdates(mlocListener);
        mlocManager = null;

    }

    /* Class My Location Listener */
    public class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            try {
                Log.e(TAG, "Check start trip: " + session.getStartTrip());
                openDb();
                new_lat = String.valueOf(roundToDecimals(loc.getLatitude(), 4));
                new_lon = String.valueOf(roundToDecimals(loc.getLongitude(), 4));
                accurcy = loc.getAccuracy();
                spd = loc.getSpeed();
                Text = String.valueOf(loc.getLatitude()) + ":" + String.valueOf(loc.getLongitude()) + ":" + session.getDistance();
                session.setText(Text);

                shw_btry_lvl = PowerConnectionReceiver.level;
                session_USER_ID = session.getUserDetails().get(session.KEY_USER_NUMERIC_ID);
                session_CUST_ACC_CODE = session.getUserDetails().get(session.KEY_CUST_ACC_CODE);
                String sessionUserName = session.getUserDetails().get(session.KEY_USER_NAME);
                String locId = session.getLocId();
                docId = sessionUserName + "_" + session_CUST_ACC_CODE;
                Date curDate = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
                final String mLastUpdateTime = dateFormat.format(curDate).toString();
                if (iLoc != null) {
                    if (!String.valueOf(iLoc.getLatitude()).isEmpty() && !String.valueOf(roundToDecimals(iLoc.getLatitude(), 4)).equals(new_lat) && !String.valueOf(iLoc.getLongitude()).isEmpty() && !String.valueOf(roundToDecimals(iLoc.getLongitude(), 4)).equals(new_lon)) {
                        float[] result = new float[3];
                        double currentLat = loc.getLatitude();
                        double currentLng = loc.getLongitude();
                        Location.distanceBetween(roundToDecimals(iLoc.getLatitude(), 6), roundToDecimals(iLoc.getLongitude(), 6), roundToDecimals(currentLat, 6), roundToDecimals(currentLng, 6), result);

                        // Distance calculation in meters
                        if (result[0] > 30) {
                            if (session.getDistance() > 0) {
                                distance = session.getDistance();
                            }
                            distance += result[0];
                            session.setDistance(distance);
                             couchDb.updateDoc(sessionUserName, String.valueOf(roundToDecimals(currentLat, 6)), String.valueOf(roundToDecimals(currentLng, 6)), distance, docId, session.getLocId());
                            iLoc = loc;
                            locationDetails = new com.traconmobi.tom.model.Location(loc.getLatitude(),loc.getLongitude(),
                                    session.getDistance(), mLastUpdateTime);
                            locationListDetails.add(locationDetails);
                            tripwiseWaypoint.put(tripKey + session.getEndTripCount(), locationListDetails);
                            couchDb.setWaypoint(tripwiseWaypoint,session_USER_ID,session_USER_NAME,session_CUST_ACC_CODE);
                            //latlngList.add("{\"Latitude\":" + currentLat + ",\"Longitude\":" +  currentLng + ",\"Distance\":" + distance + ",\"Time\":" + "\""+mLastUpdateTime+"\"" + "}");
                            latlngList.add("{\"Latitude\":" + roundToDecimals(currentLat, 6) + ",\"Longitude\":" + roundToDecimals(currentLng, 6) + ",\"Distance\":" + distance + ",\"Time\":" + "\"" + mLastUpdateTime + "\"" + "}");
                            //latlngList.add("{\"Latitude\":" + roundToDecimals(currentLat, 6) + ",\"Longitude\":" + roundToDecimals(currentLng, 6) + ",\"Distance\":" + session.getDistance() + ",\"Time\":" + "\"" + mLastUpdateTime + ",\"Trip\":" + "\"" + session.getStartTrip() + ",\"TripCount\":" + "\"" + session.getEndTripCount() + "\"" + "}");
                            // couchDb.updateWaypoint(latlngList, docId, session.getLocId(), sessionUserName, session_USER_ID);
                        }
                        Crashlytics.log(Log.ERROR, TAG, "location is not null: " + iLoc);
                    }
                    iLoc = loc;
                }
                if (iLoc == null) {
                    Crashlytics.log(Log.ERROR, TAG, "location is null");
                    iLoc = loc;
                    latlngList.add("{\"Latitude\":" + roundToDecimals(iLoc.getLatitude(), 6) + ",\"Longitude\":" + roundToDecimals(iLoc.getLongitude(), 6) + ",\"Distance\":" + distance + ",\"Time\":" + "\"" + mLastUpdateTime + "\"" + "}");
                    //latlngList.add("{\"Latitude\":" + roundToDecimals(iLoc.getLatitude(), 6) + ",\"Longitude\":" + roundToDecimals(iLoc.getLongitude(), 6) + ",\"Distance\":" + session.getDistance() + ",\"Time\":" + "\"" + mLastUpdateTime + ",\"Trip\":" + "\"" + session.getStartTrip() + ",\"TripCount\":" + "\"" + session.getEndTripCount() + "\"" + "}");
                }
                flag = true;
                shw_btry_lvl = PowerConnectionReceiver.level;

                userTravelDetails = new UserTravelDetails(session.getTimezone(), transportUsed, shw_btry_lvl, session.getDistance(), String.valueOf(session.getEndTripCount()),
                        locationDetails,"NA", session.getLocId(), startTripLocation, startTripTime);
                userTravelDetailsMap.put(tripKey + session.getEndTripCount(), userTravelDetails);
                couchDb.setUserTrackHistory(session_CUST_ACC_CODE, session_USER_ID, session_USER_NAME, userTravelDetailsMap);

                if (flag == true) {

                    Crashlytics.log(Log.ERROR, TAG, "Battery life" + shw_btry_lvl);
                    if (session_USER_ID != null && session_CUST_ACC_CODE != null) {
                        entityString2 = "/" + session_CUST_ACC_CODE + "/" + session_USER_ID + "/" + shw_btry_lvl + "/" + distance + "/" + token + "/" + session.getEndTripCount();
                        Crashlytics.log(Log.ERROR, TAG, "Loction entityString2 " + entityString2);
                        String status = "location";
                        RequestBody formdata = new FormBody.Builder()
                                .add("company_id", session_CUST_ACC_CODE)
                                .add("iemi", session.getKeyIMEI())
                                .add("token", session.getKeyToken())
                                .add("timezone", session.getTimezone())
                                .add("user_id", session.getuserId())
                                .add("battery_life", String.valueOf(shw_btry_lvl))
                                .add("distance_travel", String.valueOf(distance))
                                .add("trip_count", String.valueOf(session.getEndTripCount()))
                                .build();
                        OkHttpHandlerPost handler_gps = new OkHttpHandlerPost(formdata, URL_gps_log,status,session_DB_PATH, session_DB_PWD);
                        handler_gps.sendRequest();
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Crashlytics.log(Log.ERROR, TAG, "Exception onLocationChanged " + e.getMessage());
            } finally {
            }
        }

        public double roundToDecimals(double d, int c) {
            int temp = (int) (d * Math.pow(10, c));
            return ((double) temp) / Math.pow(10, c);
        }

        @Override
        public void onProviderDisabled(String provider) {
            try {
                turnGPSOn(getApplicationContext());
                //startService(new Intent(getApplicationContext(), GPSStatusBroadcastReceiver.class));
            } catch (SecurityException s) {
                Crashlytics.log(Log.ERROR, TAG, "Exception onProviderDisabled SecurityException " + s.getMessage());
            } catch (Exception e) {
                Crashlytics.log(Log.ERROR, TAG, "Exception onProviderDisabled " + e.getMessage());
            }
        }

        @SuppressWarnings("deprecation")
        public void turnGPSOn(Context context) {
            try {
                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");//
                intent.putExtra("enabled", true);
                context.sendBroadcast(intent);
                String provider = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if (!provider.contains("gps")) { //if gps is disabled
                    final Intent poke = new Intent();
                    poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
                    poke.addCategory(Intent.CATEGORY_SELECTED_ALTERNATIVE);
                    poke.setData(Uri.parse("3"));
                    context.sendBroadcast(poke);
                }
            } catch (SecurityException s) {
                Crashlytics.log(Log.ERROR, TAG, "Exception turnGPSOn SecurityException " + s.getMessage());
            } catch (Exception e) {
                e.getStackTrace();
                Crashlytics.log(Log.ERROR, TAG, "Exception turnGPSOn " + e.getMessage());
            }
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }
}