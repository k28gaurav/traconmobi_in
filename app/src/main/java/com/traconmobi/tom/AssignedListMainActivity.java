
package com.traconmobi.tom;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by kumargaurav on 11/10/15.
 */
public class AssignedListMainActivity extends FragmentActivity implements DeliveryMultipleFragment.OnFragmentInteractionListener, PickUpAssignedList.CommunicationChannel {

    ViewPager pager;
    AssignedListAdapter adapter;
    SlidingTabLayout tabs;

    CharSequence Titles[] = {"Delivery", "PickUp"};
    int Numboftabs = 2;
    public static String employeeId = "";
    SharedPreferences pref;
    public static final String PREF_NAME = "Pager";
    TextView title;
    int position;
    public String TAG="AssignedListMainActivity";
    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try
        {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
         //Intializing Fabric
        Fabric.with(this, new Crashlytics());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_list_main);
        getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.activity_assigned_list_main);

        title=(TextView)findViewById(R.id.txt_title);
        title.setText("ASSIGNED LIST");

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter = new AssignedListAdapter(getSupportFragmentManager(), Titles, Numboftabs, employeeId);
            session = new SessionManager(getApplicationContext());
           // PickUpScanList sca = new PickUpScanList(this);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);

        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
    }
    catch(Exception e)
    {
        Crashlytics.log(android.util.Log.ERROR, TAG, "onCreate" + e.getMessage());
    }
    }

    public void toggleMenu(View v)
    {
        Intent homeActivity = new Intent(this, HomeMainActivity.class);
        startActivity(homeActivity);
    }

    public void replaceChild(BaseFragment oldFrg, int position) {
        adapter.replaceChildFragment(oldFrg, position);
    }

    @Override
    public void onFragmentInteraction(boolean status)
    {
        try
        {
        if(status)
        {
            Fragment fragment = (Fragment) getSupportFragmentManager().
                    findFragmentByTag("android:switcher:" + R.id.pager + ":" + pager.getCurrentItem());

            if (fragment != null && fragment instanceof BaseFragment) // could be null if not instantiated yet
            {
                if (fragment.getView() != null) {
                    BaseFragment bf = (BaseFragment)fragment;
                    if(bf.isShowingChild()) {
                        replaceChild(bf, pager.getCurrentItem());
                    }
                    else {
//                        backButton();
                    }
                }
            }
        }
    }
    catch(Exception e)
    {
        Crashlytics.log(android.util.Log.ERROR, TAG, "onFragmentInteraction" + e.getMessage());
    }
    }


    public void onclk_noti(View v)
    {
        Intent homeActivity = new Intent(this, NotificationActivity.class);
        startActivity(homeActivity);
    }

    public void onclk_trip(View v)
    {
        Intent homeActivity = new Intent(this, Start_End_TripMainActivity.class);
        startActivity(homeActivity);
    }


    @Override
    public void setCommunication(int i) {
        position = i;
    }

    @Override
    public void onBackPressed() {

    }
}

