//package com.navatech.internlmodule;
//
//public class ParseHoldReason_XmlResponse {
//
//}
package com.traconmobi.tom;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map.Entry;

//import net.sqlcipher.database.SQLiteConstraintException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import android.provider.BaseColumns;
import android.util.Log;
import android.util.Xml;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteConstraintException;

public class ParseTransfer_Users_XmlResponse {

	private ArrayList<HashMap<String,String>> parsedDelCompleteList;
	protected SQLiteDatabase db;
	private StringReader xmlReader;
	
	public String trnsfr_usr_id_Value=null;
	public String trnsfr_usr_nme_Value=null;
	public String trnsfr_cust_acc_no_Value=null;
	public String trnsfr_loc_code_Value=null;
	
//	public String DEL_isPickedValue=null;

	public String TAG="ParseTransfer_Users_XmlResponse";
	
	public static String trnsfr_usr_idVal,trnsfr_cust_acc_noVal,trnsfr_usr_nmeVal,trnsfr_loc_codeVal;
	
	private final String starttagList = "USER";
	private final String trnsfr_usr_id = "USERID";
	private final String trnsfr_usr_nme = "USERNAME";
	private final String trnsfr_cust_acc_no="COMPANYID";
	private final String trnsfr_loc_code = "LOCATIONCODE";
//	private final String DEL_isDelivered = "isDelivered";	 
	
	 // Session Manager Class
    SessionManager session;
    public String session_DB_PATH,session_DB_PWD,session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME;
	// variable to hold context
    public Context context;
    protected SQLiteDatabase db_insert;

	Cursor cursor;
	
	public ParseTransfer_Users_XmlResponse(String xml,Context context)
	{
		try
		{
		xmlReader = new StringReader(xml);
		this.context=context;

		//Intializing Fabric
		Fabric.with(context, new Crashlytics());
	    // Session class instance
        session = new SessionManager(context);

        /**GETTING SESSION VALUES**/
  
        // get AuthenticateDb data from session
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
         
        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
        
        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
        
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
         
        // session_USER_LOC
        session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);	      
         
        // session_USER_NUMERIC_ID
        session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);
      
        // session_CURRENT_DT
        session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);
       
        // session_CUST_ACC_CODE
        session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);
        
        // session_USER_NAME
        session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);
        
		}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR,TAG,"Exception ParseLoginAuthenticateXmlResponse" + e.getMessage());
		}
	
//		xmlReader = new StringReader(xml);
	
	}
	
	public void parse() throws XmlPullParserException, IOException {
		try {


		TreeMap<String, String> DelCompleteListObj = null;
		XmlPullParser parser = Xml.newPullParser();
		parser.setInput(xmlReader);
		int eventType = parser.getEventType();

		DelCompleteListObj = new TreeMap<String, String>();

		parsedDelCompleteList = new ArrayList<HashMap<String, String>>();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			String xmlNodeName = parser.getName();
			if (XmlPullParser.START_TAG == eventType) {
				xmlNodeName = parser.getName();
				if (xmlNodeName.equalsIgnoreCase(trnsfr_usr_id)) {
					trnsfr_usr_id_Value = parser.nextText().toString();
//				Log.d("AWB_ID",outscan_awbidValue);			
					DelCompleteListObj.put("trnsfr_usr_id", trnsfr_usr_id_Value);
//					Crashlytics.log(android.util.Log.ERROR,TAG,"trnsfr_usr_id_Value " + trnsfr_usr_id_Value);
				}
//
				else if (xmlNodeName.equalsIgnoreCase(trnsfr_usr_nme)) {
					trnsfr_usr_nme_Value = parser.nextText().toString();
					DelCompleteListObj.put("trnsfr_usr_nme", trnsfr_usr_nme_Value);
//				Log.d("AWB_NUM",outscan_awbnoValue);				
				} else if (xmlNodeName.equalsIgnoreCase(trnsfr_loc_code)) {
					trnsfr_loc_code_Value = parser.nextText().toString();
					DelCompleteListObj.put("trnsfr_loc_code", trnsfr_loc_code_Value);
//			Log.d("AWB_NUM",outscan_awbnoValue);				
				} else if (xmlNodeName.equalsIgnoreCase(trnsfr_cust_acc_no)) {
					trnsfr_cust_acc_no_Value = parser.nextText().toString();
					DelCompleteListObj.put("trnsfr_cust_acc_no", trnsfr_cust_acc_no_Value);
//				 Crashlytics.log(android.util.Log.ERROR,TAG,"ud_cust_acc_no_Value " + ud_cust_acc_no_Value);
//				Crashlytics.log(android.util.Log.ERROR,TAG,"DEL_returnd_assgnidValue " + DEL_returnd_assgnidValue);
//				Log.d("OUTSCAN_DTTME",outscan_dttmeValue);				
				}

//			else if (xmlNodeName.equalsIgnoreCase(DEL_returnd_assgn_no)) 
//			{
//				DEL_returnd_assgn_no_Value = parser.nextText().toString();				
//				DelCompleteListObj.put("DEL_returnd_assgn_no", DEL_returnd_assgn_no_Value);				
////			Log.d("OUTSCAN_DTTME",outscan_dttmeValue);				
//			}
//			else if (xmlNodeName.equalsIgnoreCase(DEL_isDelivered)) 
//				{			
//				DEL_isPickedValue = parser.nextText().toString();			
//				DelCompleteListObj.put("DEL_isDeliveredValue", DEL_isPickedValue);			
////				Log.d("Consignee_Name",consignee_nmeValue);			
//				}			

			} else if (XmlPullParser.END_TAG == eventType) {
				if (xmlNodeName.equalsIgnoreCase(starttagList)) {
//			  Crashlytics.log(android.util.Log.ERROR,TAG,"PODListObj" + PODListObj.size());
//			  Crashlytics.log(android.util.Log.ERROR,TAG,"DELListObj" + DELListObj.size());


//			Crashlytics.log(android.util.Log.ERROR,TAG,"PODListObj value : "
//			
//			+ PODListObj.values());
//			Crashlytics.log(android.util.Log.ERROR,TAG,"DELListObj value : "
//					
//			+ DELListObj.values());

//			Crashlytics.log(android.util.Log.ERROR,TAG,"PODListObj show : " +PODListObj);
//			Crashlytics.log(android.util.Log.ERROR,TAG,"DELListObj show : " +DELListObj);

//			
//			parsedPODList.add(new HashMap<String, String>(
//			
//			PODListObj));

					parsedDelCompleteList.add(new HashMap<String, String>(

							DelCompleteListObj));

//			for (@SuppressWarnings("unused") Entry<String, String> entry : PODListObj.entrySet()) 
//			{		
//			    POD_awbid =PODListObj.get("POD_returnd_awbid");			
////			    Log.i("POD_awbid", POD_awbid);
//			    POD_awbno=PODListObj.get("POD_returnd_awbno");	
//
//			}
					for (Entry<String, String> entry : DelCompleteListObj.entrySet()) {
						trnsfr_usr_idVal = DelCompleteListObj.get("trnsfr_usr_id");
						trnsfr_usr_nmeVal = DelCompleteListObj.get("trnsfr_usr_nme");
						trnsfr_loc_codeVal = DelCompleteListObj.get("trnsfr_loc_code");
						trnsfr_cust_acc_noVal = DelCompleteListObj.get("trnsfr_cust_acc_no");

//				Crashlytics.log(android.util.Log.ERROR,TAG,"dest_loc_codeVal " + hold_reason_codeVal + hold_reason_descVal);
						ContentValues cv = new ContentValues();
//				    cv.put("T_U_ID",u_id);			
						cv.put("T_trnsfr_usr_id", trnsfr_usr_id_Value);
						cv.put("T_trnsfr_usr_nme", trnsfr_usr_nme_Value);
						cv.put("T_Loc_Cd", trnsfr_loc_code_Value);
						cv.put("T_Cust_Acc_NO", trnsfr_cust_acc_no_Value);
						cv.put("D_DCREATE_DT", session_CURRENT_DT);
//				    Crashlytics.log(android.util.Log.ERROR,TAG,"parser reason_code "+ trnsfr_usr_id_Value + ud_cust_acc_no_Value + session_CURRENT_DT  );
//				    cv.put("T_CITY_CODE",u_city_code);
//				    cv.put("T_LOC_TYPE",u_loc_type);
//				    cv.put("T_IMEI",u_IMEI_num);
//				    cv.put("T_Cust_Acc_No",Cust_acc_num);
//				    cv.put("CNAME",Cust_name);
//				    cv.put("CEMAIL",Cust_email);
//				    cv.put("D_ServerDate",serverdateValue);

						try {

							if (session_DB_PATH != null && session_DB_PWD != null) {
//					    	db_insert=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
								db_insert = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
								db_insert.beginTransaction();
								db_insert.insertWithOnConflict("Transfer_User_MASTER", BaseColumns._ID, cv, SQLiteDatabase.CONFLICT_REPLACE);
								Crashlytics.log(android.util.Log.ERROR, TAG, "TOM_Assignments transfer user " + cv);
								db_insert.setTransactionSuccessful();
								db_insert.endTransaction();
								db_insert.close();
							} else {
								Crashlytics.log(android.util.Log.ERROR, TAG, "Error Login parser session_DB_PATH is null ");
							}
						} catch (SQLiteConstraintException ex) {

							//what ever you want to do
							Crashlytics.log(android.util.Log.ERROR, TAG, "Exception session_DB_PATH " + ex.getMessage());
						} catch (Exception e) {
							Crashlytics.log(android.util.Log.ERROR, TAG, e.getMessage());
						} finally {

							if (db_insert != null && db_insert.isOpen()) {
								db_insert.close();
							}

						}
						/***For Loop close****/

					}
//			Crashlytics.log(android.util.Log.ERROR,TAG,"PODListObj value 2 : "
//			
//			+ PODListObj);

//			PODListObj.clear();

//			Crashlytics.log(android.util.Log.ERROR,TAG,parsedPODList);	

//			Crashlytics.log(android.util.Log.ERROR,TAG,"DELListObj value 2 : "
//					
//			+ DELListObj);

					DelCompleteListObj.clear();

//			Crashlytics.log(android.util.Log.ERROR,TAG,parsedDELList);
				}

			}
			eventType = parser.next();
		}
	}catch(Exception e) {
			Crashlytics.log(android.util.Log.ERROR, TAG, e.getMessage());
		}
}

@SuppressWarnings("unused")
private String readText(XmlPullParser parser) throws IOException,

    XmlPullParserException {

String result = "";

if (parser.next() == XmlPullParser.TEXT) {

    result = parser.getText();

//    Log.d("wat is the result",result);

    parser.nextTag();

}

return result;

}

public ArrayList getparsedDelCompleteList() {

return parsedDelCompleteList;

}
}


//package com.navatech.tom;
//
//public class ParseTransfer_Users_XmlResponse {
//
//}
