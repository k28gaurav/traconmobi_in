package com.traconmobi.tom;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by kumargaurav on 11/7/15.
 */
public class PickUpInCompleteStatusAdapter extends RecyclerView.Adapter<PickUpInCompleteStatusAdapter.ViewHolder> {
    private static OnItemTouchListener onItemTouchListener;
    public ArrayList<PickUpCustomData> pickUpCustomData;
    public int[] image = null;
    Context _context;
    SessionManager session;
    public String TAG="PickUpInCompleteStatusAdapter";
    public PickUpInCompleteStatusAdapter(Context ctx, ArrayList<PickUpCustomData> pickUpCustomData,OnItemTouchListener onItemTouchListener) {
        this.pickUpCustomData = pickUpCustomData;
        _context = ctx;
        this.onItemTouchListener = onItemTouchListener;
        session = new SessionManager(ctx);
    }

    @Override
    public PickUpInCompleteStatusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.layout_incomplete_pickup_status, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PickUpInCompleteStatusAdapter.ViewHolder viewHolder, int position) {
        try
        {
            if(session.getAssignType(pickUpCustomData.get(position).getmAssignNo().toString()).equalsIgnoreCase("RVP")) {
                viewHolder.waybillNo.setVisibility(View.VISIBLE);
                viewHolder.itemDesc.setVisibility(View.VISIBLE);
                viewHolder.reasonOfReturn.setVisibility(View.VISIBLE);
                viewHolder.descValue.setVisibility(View.VISIBLE);
                viewHolder.assignmentNo.setText(pickUpCustomData.get(position).getmAssignNo().toString());
                viewHolder.shipperName.setText(String.valueOf(session.getConsigneeName(pickUpCustomData.get(position).getmAssignNo().toString())));
                viewHolder.pickUptime.setText("Assigned Pickup Time : " + session.getPickTime(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.NoOfPcs.setText("No. Of Pieces : " + String.valueOf(session.getNoPcs(pickUpCustomData.get(position).getmAssignNo().toString())));
                viewHolder.assignmentType.setText(session.getAssignType(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.assignmentType.setTypeface(null, Typeface.BOLD);
                viewHolder.itemDesc.setText("Item Desc : " + session.getItemDescription(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.reasonOfReturn.setText("Reason of Return : " + session.getReasonOfReturn(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.descValue.setText("Declared Value : " + session.getDescribedValue(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.waybillNo.setText(session.getWayBillNo(pickUpCustomData.get(position).getmAssignNo().toString()) + " / " + session.getOrderNo(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.waybillNo.setTypeface(null, Typeface.BOLD);
                viewHolder.shippercity.setText(session.getShipperCity(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.shipperpin.setText(session.getShipperPin(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.reason.setVisibility(View.GONE);

            }
            else{
                viewHolder.assignmentNo.setText(pickUpCustomData.get(position).getmAssignNo().toString());
                viewHolder.shipperName.setText(pickUpCustomData.get(position).getmShipperName().toString());
                viewHolder.pickUptime.setText("Pickup Time : " + session.getPickTime(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.NoOfPcs.setText("No. Of Pieces : " + session.getNoPcs(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.shippercity.setText(session.getShipperCity(pickUpCustomData.get(position).getmAssignNo().toString()));
                viewHolder.shipperpin.setText(session.getShipperPin(pickUpCustomData.get(position).getmAssignNo().toString()));
                final String reason = session.getReason(pickUpCustomData.get(position).getmAssignNo().toString());
//        if (reason != null )
                if (!reason.isEmpty() && reason != "")
                {
                    viewHolder.reasonOfReturn.setText("Reason : " +reason);
                    //viewHolder.reason.setText("Reason: " + reason);
                } else {
                    viewHolder.reasonOfReturn.setText("Reason : ");
                }
                viewHolder.waybillNo.setVisibility(View.GONE);
                viewHolder.itemDesc.setVisibility(View.GONE);
                // viewHolder.reasonOfReturn.setVisibility(View.GONE);
                viewHolder.descValue.setVisibility(View.GONE);
                viewHolder.assignmentType.setVisibility(View.GONE);
                viewHolder.reason.setVisibility(View.GONE);
            }
        }
        catch(Exception e)

        {
            Log.e(TAG, e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return pickUpCustomData.size();
    }


    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView assignmentNo, shipperName, pickUptime, NoOfPcs, assignmentType, descValue, itemDesc, reasonOfReturn, waybillNo,shippercity, shipperpin,reason;
        public ImageView call, map, fail;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            assignmentNo = (TextView)itemLayoutView.findViewById(R.id.assignment_no);
            shipperName = (TextView)itemLayoutView.findViewById(R.id.shippername);

            pickUptime = (TextView)itemLayoutView.findViewById(R.id.pickupTime);
            NoOfPcs = (TextView)itemLayoutView.findViewById(R.id.no_of_pcs);
            assignmentType = (TextView)itemLayoutView.findViewById(R.id.assignment_type);
            descValue = (TextView)itemLayoutView.findViewById(R.id.describedvalue);
            itemDesc = (TextView)itemLayoutView.findViewById(R.id.itemdescr);
            reasonOfReturn = (TextView)itemLayoutView.findViewById(R.id.reasonofreturn);
            waybillNo = (TextView)itemLayoutView.findViewById(R.id.waybill_no);
            shippercity = (TextView)itemLayoutView.findViewById(R.id.shippercity);
            shipperpin = (TextView)itemLayoutView.findViewById(R.id.shipperpin);
            reason = (TextView)itemLayoutView.findViewById(R.id.reason);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemTouchListener.onCardViewTap(v, getPosition());
                }
            });
            call = (ImageView)itemLayoutView.findViewById(R.id.call);
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemTouchListener.onButtonCallClick(v, getPosition());
                }
            });
            map = (ImageView)itemLayoutView.findViewById(R.id.map);
            map.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemTouchListener.onButtonMapClick(v, getPosition());
                }
            });
            assignmentType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemTouchListener.onRVPTextClick(v, getPosition());
                }
            });
        }
    }
}


