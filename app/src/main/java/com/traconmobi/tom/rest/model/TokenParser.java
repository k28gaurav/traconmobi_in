package com.traconmobi.tom.rest.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by kumargaurav on 3/23/16.
 */
public class TokenParser implements Serializable {

    @SerializedName("Sync")
    private Integer Sync;

    @SerializedName("UserId")
    private Integer UserId;

    @SerializedName("UserName")
    private String UserName;

    @SerializedName("Password")
    private String Password;

    @SerializedName("CompanyID")
    private Integer CompanyID;

    @SerializedName("CompanyName")
    private String CompanyName;

    @SerializedName("LocID")
    private Integer LocID;

    @SerializedName("LocCode")
    private String LocCode;

    @SerializedName("IMEI")
    private Long IMEI;

    @SerializedName("BatteryUsage")
    private Object BatteryUsage;

    @SerializedName("LastLoginDateTime")
    private String LastLoginDateTime;

    @SerializedName("ServerDateTime")
    private String ServerDateTime;

    @SerializedName("CityId")
    private Integer CityId;

    @SerializedName("LocationTypeID")
    private Integer LocationTypeID;

    @SerializedName("ParentHub")
    private Integer ParentHub;

    @SerializedName("Token")
    private String Token;

    @SerializedName("Timezone")
    private String Timezone;

    @SerializedName("baseurl")
    private String baseurl;

    @SerializedName("pickup")
    private String pickup;

    @SerializedName("rvp")
    private String rvp;

    @SerializedName("ecom")
    private String ecom;

    @SerializedName("region_id")
    private Integer regionId;

  /*  public TokenParser(String ecom, String rvp, String pickup,
                       Integer userId, String token, String timezone, String baseurl,
                       Integer locID, Integer companyID, String userName, String password, Long IMEI
    ) {
        this.ecom = ecom;
        this.rvp = rvp;
        this.pickup = pickup;
        UserId = userId;
        Token = token;
        Timezone = timezone;
        this.baseurl = baseurl;
        LocID = locID;
        CompanyID = companyID;
        UserName = userName;
        Password = password;
        this.IMEI = IMEI;
    }*/

    /**
     *
     * @return
     * The Sync
     */
    public Integer getSync() {
        return Sync;
    }

    /**
     *
     * @param Sync
     * The Sync
     */
    public void setSync(Integer Sync) {
        this.Sync = Sync;
    }

    /**
     *
     * @return
     * The UserId
     */
    public Integer getUserId() {
        return UserId;
    }

    /**
     *
     * @param UserId
     * The UserId
     */
    public void setUserId(Integer UserId) {
        this.UserId = UserId;
    }

    /**
     *
     * @return
     * The UserName
     */
    public String getUserName() {
        return UserName;
    }

    /**
     *
     * @param UserName
     * The UserName
     */
    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    /**
     *
     * @return
     * The Password
     */
    public String getPassword() {
        return Password;
    }

    /**
     *
     * @param Password
     * The Password
     */
    public void setPassword(String Password) {
        this.Password = Password;
    }

    /**
     *
     * @return
     * The CompanyID
     */
    public Integer getCompanyID() {
        return CompanyID;
    }

    /**
     *
     * @param CompanyID
     * The CompanyID
     */
    public void setCompanyID(Integer CompanyID) {
        this.CompanyID = CompanyID;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    /**
     *
     * @return
     * The CompanyName
     */
    public String getCompanyName() {
        return CompanyName;
    }

    /**
     *
     * @param CompanyName
     * The CompanyName
     */
    public void setCompanyName(String CompanyName) {
        this.CompanyName = CompanyName;
    }

    /**
     *
     * @return
     * The LocID
     */
    public Integer getLocID() {
        return LocID;
    }

    /**
     *
     * @param LocID
     * The LocID
     */
    public void setLocID(Integer LocID) {
        this.LocID = LocID;
    }

    /**
     *
     * @return
     * The LocCode
     */
    public String getLocCode() {
        return LocCode;
    }

    /**
     *
     * @param LocCode
     * The LocCode
     */
    public void setLocCode(String LocCode) {
        this.LocCode = LocCode;
    }

    /**
     *
     * @return
     * The IMEI
     */
    public Long getIMEI() {
        return IMEI;
    }

    /**
     *
     * @param IMEI
     * The IMEI
     */
    public void setIMEI(Long IMEI) {
        this.IMEI = IMEI;
    }

    /**
     *
     * @return
     * The BatteryUsage
     */
    public Object getBatteryUsage() {
        return BatteryUsage;
    }

    /**
     *
     * @param BatteryUsage
     * The BatteryUsage
     */
    public void setBatteryUsage(Object BatteryUsage) {
        this.BatteryUsage = BatteryUsage;
    }

    /**
     *
     * @return
     * The LastLoginDateTime
     */
    public String getLastLoginDateTime() {
        return LastLoginDateTime;
    }

    /**
     *
     * @param LastLoginDateTime
     * The LastLoginDateTime
     */
    public void setLastLoginDateTime(String LastLoginDateTime) {
        this.LastLoginDateTime = LastLoginDateTime;
    }

    /**
     *
     * @return
     * The ServerDateTime
     */
    public String getServerDateTime() {
        return ServerDateTime;
    }

    /**
     *
     * @param ServerDateTime
     * The ServerDateTime
     */
    public void setServerDateTime(String ServerDateTime) {
        this.ServerDateTime = ServerDateTime;
    }

    /**
     *
     * @return
     * The CityId
     */
    public Integer getCityId() {
        return CityId;
    }

    /**
     *
     * @param CityId
     * The CityId
     */
    public void setCityId(Integer CityId) {
        this.CityId = CityId;
    }

    /**
     *
     * @return
     * The LocationTypeID
     */
    public Integer getLocationTypeID() {
        return LocationTypeID;
    }

    /**
     *
     * @param LocationTypeID
     * The LocationTypeID
     */
    public void setLocationTypeID(Integer LocationTypeID) {
        this.LocationTypeID = LocationTypeID;
    }

    /**
     *
     * @return
     * The ParentHub
     */
    public Integer getParentHub() {
        return ParentHub;
    }

    /**
     *
     * @param ParentHub
     * The ParentHub
     */
    public void setParentHub(Integer ParentHub) {
        this.ParentHub = ParentHub;
    }

    /**
     *
     * @return
     * The Token
     */
    public String getToken() {
        return Token;
    }

    /**
     *
     * @param Token
     * The Token
     */
    public void setToken(String Token) {
        this.Token = Token;
    }

    /**
     *
     * @return
     * The Timezone
     */
    public String getTimezone() {
        return Timezone;
    }

    /**
     *
     * @param Timezone
     * The Timezone
     */
    public void setTimezone(String Timezone) {
        this.Timezone = Timezone;
    }

    public String getBaseurl() {
        return baseurl;
    }

    /**
     *
     * @param baseurl
     * The baseurl
     */
    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }

    /**
     *
     * @return
     * The pickup
     */
    public String getPickup() {
        return pickup;
    }

    /**
     *
     * @param pickup
     * The pickup
     */
    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    /**
     *
     * @return
     * The rvp
     */
    public String getRvp() {
        return rvp;
    }

    /**
     *
     * @param rvp
     * The rvp
     */
    public void setRvp(String rvp) {
        this.rvp = rvp;
    }

    /**
     *
     * @return
     * The ecom
     */
    public String getEcom() {
        return ecom;
    }

    /**
     *
     * @param ecom
     * The ecom
     */
    public void setEcom(String ecom) {
        this.ecom = ecom;
    }
}
