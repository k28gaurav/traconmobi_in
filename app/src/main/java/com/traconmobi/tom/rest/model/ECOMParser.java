package com.traconmobi.tom.rest.model;

import com.google.gson.annotations.SerializedName;
import com.traconmobi.tom.model.Row2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kumargaurav on 2/9/16.
 */
public class ECOMParser {
    @SerializedName("total_rows")
    private Integer totalRows;

    @SerializedName("rows")
    private List<Row2> rows = new ArrayList<Row2>();

    /**
     *
     * @return
     * The totalRows
     */
    public Integer getTotalRows() {
        return totalRows;
    }

    /**
     *
     * @param totalRows
     * The total_rows
     */
    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }

    /**
     *
     * @return
     * The rows
     */
    public List<Row2> getRows() {
        return rows;
    }

    /**
     *
     * @param rows
     * The rows
     */
    public void setRows(List<Row2> rows) {
        this.rows = rows;
    }

}
