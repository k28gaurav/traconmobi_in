/*************************************************************/

/**********CREATED BY : ASHWINI NAIK ************************/
/********CREATED DATE : 15/04/2013 **************************/
/***********PURPOSE   : CLASS FILE TO PARSE THE NOTIFICATION WEBSERVICE VALUES******/

/************************************************************/
package com.traconmobi.tom;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteConstraintException;
//import android.database.sqlite.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteConstraintException;
import android.provider.BaseColumns;
import android.util.Xml;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class ParseXmlResponsenoti {

	protected SQLiteDatabase db;
	private ArrayList<HashMap<String, String>> parsedNotifyList;
	private StringReader xmlReader;
	
//	private final String NotifyList = "Table";
//	private final String notify_awbno = "CAWB_NO";
//	private final String notify_msg = "CMESSAGE";
//	private final String notify_trans_dt_tm = "TRANS_DATE_TIME";
//	private final String notifier_nm = "NOTIFIER_NAME";
//	private final String notifier_loc = "NOTIFIER_LOC";
//	private final String notifier_dept="NOTIFIER_DEPT";
//	private final String syn_dt_tm = "SYNC_DATE_Time";
//	private final String notify_isread ="IS_Read_STATUS";
//	private final String notify_id ="CNOT_CD";	
//	private final String notify_usrid="U_ID";
	
	private final String NotifyList = "Table";
	private final String notify_awbno = "CASSIGNMENT_NO";
	private final String notify_msg = "CMESSAGE";
	private final String notify_trans_dt_tm = "TRANS_DATE_TIME";
	private final String notifier_nm = "NOTIFIER_NAME";
	private final String notifier_loc = "NOTIFIER_LOC";
	private final String cust_code="NCUST_ACC_NO";
	
	private final String syn_dt_tm = "SYNC_DATE_Time";
	private final String notify_isread ="IS_Read_STATUS";
	private final String notify_id ="CNOT_CD";	
	private final String notify_usrid="NUID";
	private final String notify_assgn_type="ASSIGN_TYPE";
//	public String notify_awb_value=null;
//	public String notify_msg_value=null;
//	public String notify_trans_dt_value=null;
//	public String notifier_name_value=null;
//	public String notifier_loc_value=null;
//	public String notifier_dept_value=null;
//	public String notify_sync_dt_value=null;
//	public String notify_isread_value=null;
//	public String notify_id_value=null;
//	public String notify_usrid_value=null;
	
	public String notify_awb_value=null;
	public String notify_msg_value=null;
	public String notify_trans_dt_value=null;
	public String notifier_name_value=null;
	public String notifier_loc_value=null;
	public String cust_code_value=null;
	public String notify_sync_dt_value=null;
	public String notify_isread_value=null;
	public String notify_id_value=null;
	public String notify_usrid_value=null;
	public String notify_assgn_type_value=null;
	public static String notify_user_id;
	
	Cursor c_show_noti_cnt;
	public static String assigned_noti_count;
	
	// Session Manager Class
	   SessionManager session;
	   public String session_DB_PATH,session_DB_PWD;
		// variable to hold context
	   public Context context;
	public String TAG="ParseXmlResponsenoti";
	public ParseXmlResponsenoti(String xml,Context context) 
	{
		try
		{
		xmlReader = new StringReader(xml);
		
		this.context=context;

		//Intializing Fabric
		Fabric.with(context, new Crashlytics());
	    // Session class instance
        session = new SessionManager(context);

        /**GETTING SESSION VALUES**/
        
        // get AuthenticateDb data from session
        HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();
         
        // DB_PATH
        session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);
    
        // DB_PWD
        session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);
		}
		catch(Exception e)
		{
			e.getStackTrace();
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception ParseXmlResponsenoti " + e.getMessage());
		}
	}

	@SuppressLint("SimpleDateFormat")
	public void parse() throws XmlPullParserException, IOException 
	{
		try
		{
	        
		TreeMap<String, String> notifyListObj;
		XmlPullParser parser = Xml.newPullParser();
		parser.setInput(xmlReader);
		int eventType = parser.getEventType();

		notifyListObj = new TreeMap<String, String>();
		parsedNotifyList = new ArrayList<HashMap<String, String>>();
		while (eventType != XmlPullParser.END_DOCUMENT) {
		String xmlNodeName = parser.getName();
					
		if (XmlPullParser.START_TAG == eventType) 
		{
			xmlNodeName = parser.getName();	
			if (xmlNodeName.equalsIgnoreCase(notify_awbno))
			{			
				notify_awb_value = parser.nextText().toString();
//				Log.d("Notify AWB_ID",notify_awb_value);
				notifyListObj.put("notify_awbno", notify_awb_value);				
			}
			else if (xmlNodeName.equalsIgnoreCase(notify_msg)) 
				{					
					notify_msg_value = parser.nextText().toString();
					notifyListObj.put("notify_msg", notify_msg_value);
//					Log.d("Notify msg",notify_msg_value);					
				}
			else if (xmlNodeName.equalsIgnoreCase(notify_assgn_type)) 
			{					
				notify_assgn_type_value = parser.nextText().toString();
				notifyListObj.put("notify_assgn_type", notify_assgn_type_value);
//				Log.d("Notify msg",notify_msg_value);					
			}
			else if (xmlNodeName.equalsIgnoreCase(notify_trans_dt_tm))
				{				
					notify_trans_dt_value = parser.nextText().toString();
					notifyListObj.put("notify_trans_dt_tm", notify_trans_dt_value);
//					Log.d("Notify Trans date",notify_trans_dt_value);
				}
			else if (xmlNodeName.equalsIgnoreCase(notifier_nm)) 
				{
					notifier_name_value = parser.nextText().toString();
					notifyListObj.put("notifier_nm", notifier_name_value);
//					Log.d("Notifier Name",notifier_name_value);
				}
			else if (xmlNodeName.equalsIgnoreCase(notifier_loc)) 
				{
					notifier_loc_value = parser.nextText();
					notifyListObj.put("notifier_loc", notifier_loc_value);
//					Log.d("Notifier location",notifier_loc_value);
				}
			
			else if (xmlNodeName.equalsIgnoreCase(cust_code)) 
			{
				cust_code_value = parser.nextText();
				notifyListObj.put("cust_code", cust_code_value);
//				Log.d("Notifier location",notifier_loc_value);
			}
			else if (xmlNodeName.equalsIgnoreCase(syn_dt_tm)) 
				{				
					notify_sync_dt_value = parser.nextText();
					notifyListObj.put("syn_dt_tm", notify_sync_dt_value);
//					Log.d("Notify sync date",notify_sync_dt_value);
				}
			else if (xmlNodeName.equalsIgnoreCase(notify_isread)) 
				{
					notify_isread_value = parser.nextText().toString();
					notifyListObj.put("notify_isread", notify_isread_value);
//					Log.d("Notify status",notify_isread_value);
				}
			else if (xmlNodeName.equalsIgnoreCase(notify_id)) 
				{
					notify_id_value = parser.nextText().toString();
					notifyListObj.put("notify_id", notify_id_value);
//					Log.d("notify_id",notify_id_value);
				}
			else if (xmlNodeName.equalsIgnoreCase(notify_usrid)) 
			{
				notify_usrid_value = parser.nextText().toString();
				notifyListObj.put("notify_usrid", notify_usrid_value);
//				Log.d("notify_usrid",notify_usrid_value);
			}
		  } else if (XmlPullParser.END_TAG == eventType) 
		  		 {
					if (xmlNodeName.equalsIgnoreCase(NotifyList)) 
					{
						parsedNotifyList.add(new HashMap<String, String>(notifyListObj));
						for (Entry<String, String> entry : notifyListObj.entrySet()) 
						{					
					    String Notify_awb_Number =notifyListObj.get("notify_awbno");
					    String notifier_msg=notifyListObj.get("notify_msg");
					    String trans_dt_tm=notifyListObj.get("notify_trans_dt_tm");
//					    Crashlytics.log(android.util.Log.ERROR,TAG,"trans_dt_tm "+ trans_dt_tm);
//					    String dateReceivedFromUser = "12/13/2012"; 
					    String dateReceivedFromUser =trans_dt_tm;
//					    Crashlytics.log(android.util.Log.ERROR,TAG,"dateReceivedFromUser "+ dateReceivedFromUser);
				        DateFormat userDateFormat = new SimpleDateFormat("yyyy-mm-dd");  
				        DateFormat dateFormatNeeded = new SimpleDateFormat("dd-mm-yyyy");  
				        Date date = null;
						try {
							date = userDateFormat.parse(dateReceivedFromUser);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}  
				        String convertedDate = dateFormatNeeded.format(date);  
				        
//				        Crashlytics.log(android.util.Log.ERROR,TAG,"convertedDate" + convertedDate);
//					    Crashlytics.log(android.util.Log.ERROR,TAG,"actual value of transdate" +trans_dt_tm);
					    String Transfer_date_time=trans_dt_tm.substring(11, 16);
//					    Crashlytics.log(android.util.Log.ERROR,TAG,"The transferd notification date"+Transfer_date_time);
					    String notifier_name=notifyListObj.get("notifier_nm");
					    String notifier_location=notifyListObj.get("notifier_loc");
					    String notifier_cust_code=notifyListObj.get("cust_code");
					    String notify_sync_datetime=notifyListObj.get("syn_dt_tm");
					    String notify_sync_dt=notify_sync_datetime.substring(0, 10);
					    String notify_sync_tm=notify_sync_datetime.substring(11, 19);
					    String notify_status=notifyListObj.get("notify_isread");
					    String notify_id_num=notifyListObj.get("notify_id");
					    notify_user_id=notifyListObj.get("notify_usrid");				
					    String notify_assgn_typ=notifyListObj.get("notify_assgn_type");	
//					    ContentValues cv =new ContentValues();
//					    cv.put("U_ID",notify_user_id);
//					    cv.put("CAWB_NO",Notify_awb_Number);
//					    cv.put("MESSAGE",notifier_msg);
//					    cv.put("TRANS_DATE_Time",convertedDate +" "+Transfer_date_time);
//					    cv.put("NOTIFIER_NAME",notifier_name);
//					    cv.put("NOTIFIER_LOC",notifier_location);
//					    cv.put("NOTIFIER_DEPT",notifier_deptmnt);
//					    cv.put("SYNC_DATE",notify_sync_dt);
//					    cv.put("SYNC_Time", notify_sync_tm);
//					    cv.put("IS_Read_STATUS",notify_status);
//					    cv.put("CNOT_CD",notify_id_num);
					
					    ContentValues cv =new ContentValues();
					    cv.put("T_U_ID",notify_user_id);
					    cv.put("T_Assignment_Number",Notify_awb_Number);
					    cv.put("T_Message",notifier_msg);
					    cv.put("D_Trans_Date_Time",convertedDate +" "+Transfer_date_time);
					    cv.put("T_Notifier_Name",notifier_name);
					    cv.put("T_Notifier_Loc",notifier_location);
					    cv.put("T_Cust_Acc_NO",notifier_cust_code);
					    cv.put("D_Sync_Date",notify_sync_dt);
					    cv.put("D_Sync_Time", notify_sync_tm);
					    cv.put("C_is_Read_Status",notify_status);
					    cv.put("T_Notify_Code",notify_id_num);
					    cv.put("T_Assignment_Type", notify_assgn_typ);
					    
					    try{
					    	
					    	if(session_DB_PATH != null && session_DB_PWD != null)
					      	  {
//						    	db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
						    	db=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
						    	db.beginTransaction();	
						    	db.insertWithOnConflict("TOM_NOTIFICATION_TRN", BaseColumns._ID, cv, SQLiteDatabase.CONFLICT_IGNORE);
						    	db.setTransactionSuccessful();
						    	db.endTransaction();
						    	db.close();
			              	  }
					    	else
							{
								Crashlytics.log(android.util.Log.ERROR, TAG, "Error Notification parser TOMLoginUserActivity.file is null ");
							}
	       				}
	       				catch(SQLiteConstraintException ex){
	       				//what ever you want to do
	       				}

					}					
					notifyListObj.clear();										
				}
			}
			eventType = parser.next();			
		}	
		}
		catch(Exception e)
		  {
			  e.printStackTrace();
		  }
		  finally 
		  {
			// This block contains statements that are ALWAYS executed
						// after leaving the try clause, regardless of whether we leave it:
						// 1) normally after reaching the bottom of the block;
						// 2) because of a break, continue, or return statement;
						// 3) with an exception handled by a catch clause above; or
						// 4) with an uncaught exception that has not been handled.
						// If the try clause calls System.exit(), however, the interpreter
						// exits before the finally clause can be run.
//				  if(c_show_noti_cnt!=null)
//				  {
//					  c_show_noti_cnt.close();
//				  }
//		    	if(db !=null)
//		    	{
//		    		db.close();
//		    	}
		    	
		 }
	}
	@SuppressWarnings("unused")
	private String readText(XmlPullParser parser) throws IOException,XmlPullParserException 
	{
		String result = "";
		try
		{
		if (parser.next() == XmlPullParser.TEXT) {
		    result = parser.getText();
//		    Log.d("wat is the result",result);
		    parser.nextTag();
		}
		}
		catch(Exception e)
		{
			Crashlytics.log(android.util.Log.ERROR, TAG, "Exception ParseXmlResponsenoti readText " + e.getMessage());
		}
		return result;
}
	
	public ArrayList getParsedNotifyList() {
		return parsedNotifyList;
	}
}
