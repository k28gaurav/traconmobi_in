package com.traconmobi.tom;

import android.app.ListFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;


public class AssignmentInCompleteAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    String ord_no;
    ArrayList<Fragment> fragments = new ArrayList<Fragment>();

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public AssignmentInCompleteAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, String ordrno) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        this.ord_no = ordrno;
    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0) // if the position is 0 we are returning the First tab
        {
            DeliveryIncompleteStatus tab1 = new DeliveryIncompleteStatus();
            return tab1;

        }
        else            // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            PickUpIncompleteStatus pickUpIncompleteStatus = new PickUpIncompleteStatus();
            return pickUpIncompleteStatus;
        }
        //return fragments.get(position);
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        //return fragments.size();
        return NumbOfTabs;
    }
}