package com.traconmobi.tom;

//package com.navatech.internlmodule;
//
//public class ParseHoldReason_XmlResponse {
//
//}

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Xml;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.util.Log;
import com.crashlytics.android.Crashlytics;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import io.fabric.sdk.android.Fabric;


public class ParsePickUpReason_XmlResponse {

    private ArrayList<HashMap<String,String>> parsedDelCompleteList;
    protected SQLiteDatabase db;
    private String xmlReader;

    public CouchBaseDBHelper dbHelper;

    SharedPreferences.Editor editor;
    public static final String PREF_NAME = "PickupCancel";
    public String pickup_reason_code_Value=null;
    public String pickup_cust_acc_no_Value=null;

//	public String DEL_isPickedValue=null;

    public static String pickup_reason_codeVal,pickup_cust_acc_noVal;

    private final String starttagList = "pickupReason";
    private final String pickup_reason_code = "REASON";
    private final String pickup_cust_acc_no="CUST_ACC_NO";
//	private final String DEL_isDelivered = "isDelivered";

    // Session Manager Class
    SessionManager session;
    public String session_DB_PATH,session_DB_PWD,session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME;
    // variable to hold context
    public Context context;
    public List<String> pickUpReasonData;
    public List<String> pickUpReasonId;
    public Map<String, Object> couchData;
    SharedPreferences pref;
    Cursor cursor;
    public String TAG="ParsePickUpReason_XmlResponse";
    public ParsePickUpReason_XmlResponse(String xml, Context context)
    {
        try
        {
            xmlReader = xml;
            this.context=context;
            //Intializing Fabric
            Fabric.with(context, new Crashlytics());
            // Session class instance
            session = new SessionManager(context);
            pickUpReasonData = new ArrayList<String>();
            pickUpReasonId = new ArrayList<String>();
            couchData = new HashMap<String, Object>();

            /**GETTING SESSION VALUES**/
        }
        catch(Exception e)
        {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR,TAG, "Exception ParseLoginAuthenticateXmlResponse" + e.getMessage());
        }

//		xmlReader = new StringReader(xml);

    }

    public void parse() throws Exception {

        Document document = loadXMLFromString(xmlReader);

        document.getDocumentElement().normalize();
        Element root = document.getDocumentElement();

        Crashlytics.log(android.util.Log.ERROR,TAG, "Root:" + root.getNodeName());

        NodeList nList = document.getElementsByTagName("REASON");
        Crashlytics.log(android.util.Log.ERROR,TAG, "============================");

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node node = nList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                //Print each employee's detail
                Element eElement = (Element) node;
              //  Log.e(TAG, "Reason descr" + eElement.getElementsByTagName("REASON_DESC").item(0).getTextContent());
                pickUpReasonData.add(eElement.getElementsByTagName("REASON_DESC").item(0).getTextContent());
                session.setKeyReasonId(eElement.getElementsByTagName("REASON_DESC").item(0).getTextContent(),String.valueOf(eElement.getElementsByTagName("REASON_ID").item(0).getTextContent()));
            }
        }

        Set<String> pickupId;
        Set<String> reasonId;
        pickupId = new HashSet<>(pickUpReasonData);
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putStringSet("reasonlistforcancel", pickupId);
        // editor.putStringSet("reasonId",reasonId);
        // editor.putString("assignId", couchData.get("ASSIGN_ID").toString());
        editor.commit();

        Log.e(TAG, "Check preference data" + pref.getStringSet("reasonlistforcancel", null));
    }


    public static Document loadXMLFromString(String xml) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }
}
