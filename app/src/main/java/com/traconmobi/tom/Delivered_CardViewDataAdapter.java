package com.traconmobi.tom;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by LENOVO on 11/9/2015.
 *//*
public class Delivered_CardViewDataAdapter {
}*/


        import android.content.Context;
        import android.media.Image;
        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.Filter;
        import android.widget.Filterable;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

        import java.util.ArrayList;

public class Delivered_CardViewDataAdapter extends RecyclerView.Adapter<Delivered_CardViewDataAdapter.ViewHolder> implements Filterable {
    public ArrayList<String> _assignmentNo;
    public ArrayList<String> _assignmentId;
    public  ArrayList<String> _shipperName;
    //    public ArrayList<String> _consigneeAdd1;
//    public ArrayList<String> _consigneeAdd2;
    public ArrayList<String> _shipperAccNo;
    public ArrayList<String> _pickUptime;
    public ArrayList<String> _noOfPcs;
    public int[] image = null;
    Context _context;
    ContactsFilter mContactsFilter;

    // Provide a suitable constructor (depends on the kind of dataset)
   /* public CardViewDataAdapter(Context ctx, ArrayList<String> mAssignmentNo, ArrayList<String> mShipperName, ArrayList<String> mConsigneeAdd1,
                               ArrayList<String> mConsigneeAdd2, ArrayList<String> mShipperAccNo, ArrayList<String> pickUptime, ArrayList<String> noOfPcs,
                               int[] im) {*/
    public Delivered_CardViewDataAdapter(Context ctx, String mAssignmentNo, String mAssignmentId,String mShipperName, String mShipperAccNo, String pickUptime, String noOfPcs,
                               int[] im) {
      String  _assignmentNo = mAssignmentNo;
        String  _shipperName = mShipperName;
        String  _assignmentId = mAssignmentId;
//        _consigneeAdd1 = mConsigneeAdd1;
//        _consigneeAdd2 = mConsigneeAdd2;
        String  _shipperAccNo = mShipperAccNo;
        String  _pickUptime = pickUptime;
        String  _noOfPcs = noOfPcs;
        image = im;
        _context = ctx;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Delivered_CardViewDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.delivered_cardview_layout, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData

        //viewHolder.tvtinfo_text.setText(mDataset.get(position).toString());
        /*viewHolder.fail.setImageResource(image[2]);
        viewHolder.map.setImageResource(image[1]);
        viewHolder.call.setImageResource(image[0]);*/
        viewHolder.assignmentNo.setText(_assignmentNo.get(position).toString());
        viewHolder.assignmentNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(_context, "You have selected option:" + viewHolder.assignmentNo,Toast.LENGTH_SHORT).show();
            }
        });
//        viewHolder.assignmentId.setText(_assignmentId.get(position).toString());
        viewHolder.shipperName.setText(_shipperName.get(position).toString());
//        viewHolder.consigneeAdd1.setText(_consigneeAdd1.get(position).toString());
//        viewHolder.consigneeAdd2.setText(_consigneeAdd2.get(position).toString());
        viewHolder.shipperAccNo.setText(_shipperAccNo.get(position).toString());
        viewHolder.pickUptime.setText(_pickUptime.get(position).toString());
        viewHolder.NoOfPcs.setText(_noOfPcs.get(position).toString());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return _assignmentNo.size();
    }

    @Override
    public Filter getFilter() {
        if(mContactsFilter == null) {
            mContactsFilter = new ContactsFilter();
        }
        return mContactsFilter;
    }

    // inner class to hold a reference to each item of RecyclerView
    public static class ViewHolder extends RecyclerView.ViewHolder {

        /*public TextView assignmentNo, shipperName, consigneeAdd1, consigneeAdd2, shipperAccNo, pickUptime, NoOfPcs;*/
        public TextView assignmentNo, shipperName, shipperAccNo, pickUptime, NoOfPcs;
        public ImageView call, map, fail;


        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            assignmentNo = (TextView)itemLayoutView.findViewById(R.id.tv_awb);
            shipperName = (TextView)itemLayoutView.findViewById(R.id.tv_firstName);
//            consigneeAdd1 = (TextView)itemLayoutView.findViewById(R.id.consignee_add1);
//            consigneeAdd2 = (TextView)itemLayoutView.findViewById(R.id.consignee_add2);
            shipperAccNo = (TextView)itemLayoutView.findViewById(R.id.tv_lastName);
            pickUptime = (TextView)itemLayoutView.findViewById(R.id.tv_address);
            NoOfPcs = (TextView)itemLayoutView.findViewById(R.id.tv_city);

         /* call = (ImageView)itemLayoutView.findViewById(R.id.call);
            map = (ImageView)itemLayoutView.findViewById(R.id.map);
            fail = (ImageView)itemLayoutView.findViewById(R.id.fail);*/
            //image = new int[] {R.drawable.call, R.drawable.map, R.drawable.fail};

        }
    }

    public class ContactsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // Create a FilterResults object
            FilterResults results = new FilterResults();

            // If the constraint (search string/pattern) is null
            // or its length is 0, i.e., its empty then
            // we just set the `values` property to the
            // original contacts list which contains all of them
            if (constraint == null || constraint.length() == 0) {
                results.values = _assignmentNo;
                results.count = _assignmentNo.size();
            }
            else {
                // Some search copnstraint has been passed
                // so let's filter accordingly
                ArrayList<String> filteredContacts = new ArrayList<String>();

                // We'll go through all the contacts and see
                // if they contain the supplied string
                for (String c : _assignmentNo) {
                    if (c.toUpperCase().contains( constraint.toString().toUpperCase() )) {
                        // if `contains` == true then add it
                        // to our filtered list
                        filteredContacts.add(c);
                    }
                }

                // Finally set the filtered values and size/count
                results.values = filteredContacts;
                results.count = filteredContacts.size();
            }

            // Return our FilterResults object
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
            // Now we have to inform the adapter about the new list filtered

            _assignmentNo = (ArrayList<String>) results.values;
            //planetList = (List<Planet>) results.values;
            notifyDataSetChanged();

        }
    }
}

