package com.traconmobi.tom.http;

import android.util.Log;

import com.traconmobi.tom.RequestCompanyReason;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by kumargaurav on 5/24/16.
 */

public class OkHttpHandler {
    String response = null;
    String url;

    public OkHttpHandler(String url) {
        this.url = url;
    }

    public String sendRequest () {
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response response1 = client.newCall(request).execute();
            response = response1.body().string();
            Log.e("Response OkHttp " , response);
            if(response1.code() == 200) {
                return response;
            }
        }catch (IOException e) {

        }catch(Exception e) {

        }
        return  null;
    }

}
