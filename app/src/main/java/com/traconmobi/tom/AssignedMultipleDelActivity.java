package com.traconmobi.tom;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import org.kobjects.base64.Base64;

import com.crashlytics.android.Crashlytics;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;

import android.app.ProgressDialog;
import android.database.sqlite.SQLiteConstraintException;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.Media;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import io.fabric.sdk.android.Fabric;

public class AssignedMultipleDelActivity extends Activity implements OnMultipleSaveData, MultiCODDelListener {

    // The minimum distance to change Updates in meters
//    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = (float) 11.1 ; // 10 meters
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    /* private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * randBetween(0, 5) ;*/
    Location location; // location
    OnMultipleSaveData listener;
    String Receiver;
    String Receiver_num;
    MultiCODDelListener listener1;

    // Session Manager Class
    SessionManager session;
    TinyDB tiny;
    String session_COD_AMT, session_AWB_COUNT, session_RESPONSE_AWB_NUM, result, imei_num, entityString_gps;
    public static String session_DB_PATH;
    public static String session_DB_PWD;
    TextView awb_total_cnt, cod_total;
    public static String tempDir;
    final String curnt_dt = TOMLoginUserActivity.currentdate;
    public int count = 1;
    protected Button imageview;
    static final String EXTRA_MESSAGE = null, EXTRA_MESSAGE1 = null, EXTRA_MESSAGE2 = null, DEL_UPDATE_ASSGN_ID = null;
    public String current = null;
    private Bitmap mBitmap;
    static SQLiteDatabase db;
    static String new_lat, new_lon, s1, s2, s3, bb, room = null, awb = "", emp_indx = "", COD_Amount, str_amt, st, sign1;
    String showtime, showdate;
    String BA1 = "null";
    double amt;
    static LinearLayout mContent;
    static LinearLayout ll;
    public static String employeeId, name, photoname = null;
    signature mSignature, btnsave;
    Button mClear, cancelButton;
    static Button mGetDelivered, btnsgn, btnreset, btnpicture;
    View mView;
    static File mypath;
    Cursor crs, cursor, c_routesyn, c_chk_custmr;
    String e_indx = "";
    private String uniqueId;

    private boolean clicked = false; // this is a member variable

    static ImageView iv;
    byte[] HRHK, BA;
    static File image;
    Uri ImageUri;
    public static EditText received_by, receiver_contact_num;
    public static int mYear, mMonth, mDay, hour, minute, shw_cnt;
    String PageName, PageName1;
    Bundle bundle;
    TextView version_name;
    Spinner s;
    static com.traconmobi.tom.CaptureSignature.signature msign;
    static String ssign;
    private String selectedImagePath;
    private ImageView img;
    static Bitmap theImage;
    String currentTime, photo_dt, photo_tme;

    Uri imageUri = null;
    File fileimage = null;
    final int TAKE_PHOTO_CODE = 1;
    private static final int SIGNATURE_ACTIVITY = 0;
    @SuppressWarnings("unused")
    private static final int multiple = 0;
    public String relation_type, SESSION_TRANSPORT, consgnee, get_assgn_id, get_lat, get_lng, session_user_id, session_user_pwd, session_USER_LOC, session_USER_NUMERIC_ID, session_CURRENT_DT, session_CUST_ACC_CODE, session_USER_NAME;

    protected static final String MULTIPLE_DEL_UPDATE_ASSGN_ID = null;
    protected static final String FORM_TYPE = null;

    public String FORM_TYPE_VAL;
    int count_custmr;
    byte[] BAvalue;
    public static String getphotobytearray, s_photo;
    Cursor cr, c_chk;
    public static String RoomType;
    public String TAG = "AssignedMultipleDelActivity";
    TextView title;
    Button btn;


    public static int randBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public double roundToDecimals(double d, int c) {
        int temp = (int) (d * Math.pow(10, c));
        return ((double) temp) / Math.pow(10, c);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            super.onCreate(savedInstanceState);
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            setContentView(R.layout.pod_update_layout);
            getWindow().setFeatureInt(Window.FEATURE_NO_TITLE, R.layout.pod_update_layout);

            title = (TextView) findViewById(R.id.txt_title);
            title.setText("POD UPDATE");

            listener = this;
            listener1 = this;
       
        /* Use the LocationManager class to obtain GPS locations */

            try {
                LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                LocationListener mlocListener = new MyLocationListener();


                if (location == null) {
                    mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, mlocListener);

                    if (mlocManager != null) {
                        location = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        new_lat = String.valueOf(roundToDecimals(location.getLatitude(), 4));
                        new_lon = String.valueOf(roundToDecimals(location.getLongitude(), 4));
                    }
                }
            } catch (Exception e) {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "LocationManager" + e.getMessage());
            }

            // Session class instance
            session = new SessionManager(getApplicationContext());
            tiny = new TinyDB(getApplicationContext());
            // get AuthenticateDb data from session
            HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

            // DB_PATH
            session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

            // DB_PWD
            session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);


            // get AssgnMultiple_num_Dts data from session
            HashMap<String, String> AssgnMultiple_num_Dts = session.getAssgnMultiple_num_Details();

            // session_RESPONSE_AWB_NUM
            session_RESPONSE_AWB_NUM = AssgnMultiple_num_Dts.get(SessionManager.KEY_RESPONSE_AWB_NUM);

            // get AuthenticateDb data from session
            HashMap<String, String> AssgnMultiple_Dts = session.getAssgnMultipleDetails();

            // session_AWB_COUNT
            session_AWB_COUNT = AssgnMultiple_Dts.get(SessionManager.KEY_AWB_COUNT);

            // session_COD_AMT
            session_COD_AMT = AssgnMultiple_Dts.get(SessionManager.KEY_COD_AMT);


            // get user data from session
            HashMap<String, String> login_Dts = session.getLoginDetails();

            // Userid
            session_user_id = login_Dts.get(SessionManager.KEY_UID);

            // pwd
            session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);

            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // session_USER_LOC
            session_USER_LOC = user.get(SessionManager.KEY_USER_LOC);

            // session_USER_NUMERIC_ID
            session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

            // session_CURRENT_DT
            session_CURRENT_DT = user.get(SessionManager.KEY_CURRENT_DT);

            // session_CUST_ACC_CODE
            session_CUST_ACC_CODE = user.get(SessionManager.KEY_CUST_ACC_CODE);

            // session_USER_NAME
            session_USER_NAME = user.get(SessionManager.KEY_USER_NAME);

            SESSION_TRANSPORT = tiny.getString("transport_val");

            Intent intent = getIntent();
            FORM_TYPE_VAL = intent.getStringExtra("FORM_TYPE4_VAL");

            img = (ImageView) findViewById(R.id.sign);

            received_by = (EditText) findViewById(R.id.txtRecvBy);
            receiver_contact_num = (EditText) findViewById(R.id.txtcontct_no);
            s = (Spinner) findViewById(R.id.spinnerDel);
            String shw[] = session_RESPONSE_AWB_NUM.split(",");
            for (int i = 0; i < shw.length; i++) {
                result = shw[i];
                result = result.replace("/", "");
                result = result.replace(" ", "");
                result = result.replace("-", "");
                result = result.replace(":", "");
            }

            uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_" + result;   // + "_" + Math.random();
            current = result + ".png";
            mypath = new File(Environment.getExternalStorageDirectory()
                    + "/Android/data/com.traconmobi.in/", "Signatures" + "/" + current);//new File( Environment.getExternalStorageDirectory(), "Signatures" + "/" + current);   //File(tempDir,current);

            mContent = (LinearLayout) findViewById(R.id.linearLayout);
            mSignature = new signature(this, null);
            mContent.addView(mSignature, LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);

            mGetDelivered = (Button) findViewById(R.id.btnsave);
            btnsgn = (Button) findViewById(R.id.signature_title);

            btnpicture = (Button) findViewById(R.id.picture);
            mView = mContent;

            imageview = (Button) this.findViewById(R.id.picture);
            imageview.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clicked) {
                        takePhoto();

                    } else {
                        takePhoto();
                    }

                    clicked = true;
                }
            });

            cancelButton = (Button) findViewById(R.id.btncancel);

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
            Date currentLocalTime = cal.getTime();
            DateFormat date1 = new SimpleDateFormat("HH:mm:ss z");
            date1.setTimeZone(TimeZone.getTimeZone("GMT"));
            String localTime = date1.format(currentLocalTime);

//      /** Create an ArrayAdapter using the string array and a default spinner layout*/
//         ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                 R.array.relationship, android.R.layout.simple_spinner_item);

            ArrayList<String> list = new ArrayList<String>();

            db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
            cr = db.rawQuery("select Distinct T_RELATION_TYP,_id from TBL_Relation_mstr group by T_RELATION_TYP", null);
            //ORDER BY T_Hold_Reason_Code DESC
            while (cr.moveToNext()) {
                relation_type = cr.getString(cr.getColumnIndex("T_RELATION_TYP"));
                list.add(relation_type);
            }
            list.add("SELECT RELATIONSHIP");
            final int listsize = list.size() - 1;

            /**************ArrayAdapter for spinner********************/
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, list) {
                @Override
                public int getCount() {
                    return (listsize); // Truncate the list
                }
            };
            /** Specify the layout to use when the list of choices appears*/
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            /** Apply the adapter to the spinner*/

            s.setAdapter(adapter);
            s.setSelection(listsize); // Hidden item to appear in the spinner
            s.setOnItemSelectedListener(new Listener_Of_Selecting_Room_Spinner());

            cancelButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_MULTIPLE_DEL")) {
//	    		Toast.makeText(AssignedMultipleDelActivity.this,"Record Saved succesfully", Toast.LENGTH_LONG).show();
                        Intent goToNextActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
                        startActivity(goToNextActivity);
                    }
                    if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_MULTIPLE_UNDEL")) {
//	    		Toast.makeText(AssignedMultipleDelActivity.this,"Record Saved succesfully", Toast.LENGTH_LONG).show();
                        Intent goToNextActivity = new Intent(getApplicationContext(), AssignmentInCompleteStatus.class);
                        startActivity(goToNextActivity);
                    }
                }
            });
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "onCreate" + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "onCreate UnsatisfiedLinkError" + err.getMessage());
        }
    }

    @Override
    public void onSave() {
        new CursorTask().execute();
    }

    @Override
    public void saveCODDetails() {
        new DelSavedTask().execute();
    }

    /* Class My Location Listener */
    public class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            new_lat = String.valueOf(roundToDecimals(loc.getLatitude(), 4));

            new_lon = String.valueOf(roundToDecimals(loc.getLongitude(), 4));
        }

        @Override

        public void onProviderDisabled(String provider)

        {
        }

        @Override

        public void onProviderEnabled(String provider)

        {

//   	Toast.makeText( getApplicationContext(),"Gps Enabled",Toast.LENGTH_SHORT).show();

        }

        @Override

        public void onStatusChanged(String provider, int status, Bundle extras)

        {

        }

    }/* End of Class MyLocationListener */


    private String getTodaysDate() {

        final Calendar c = Calendar.getInstance();
        int todaysDate = (c.get(Calendar.YEAR) * 10000) +
                ((c.get(Calendar.MONTH) + 1) * 100) +
                (c.get(Calendar.DAY_OF_MONTH));
        Log.w("DATE:", String.valueOf(todaysDate));
        return (String.valueOf(todaysDate));

    }

    private String getCurrentTime() {

        final Calendar c = Calendar.getInstance();

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm:ss z");
        date.setTimeZone(TimeZone.getTimeZone("GMT"));
        String localTime = date.format(currentLocalTime);


        currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
                (c.get(Calendar.MINUTE)) + ":" +
                (c.get(Calendar.SECOND));
        Log.w("TIME:", String.valueOf(currentTime) + currentTime);
        return (String.valueOf(currentTime));

    }

    public class signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void save(View v) {
            try {
                if (mBitmap == null) {
                    mBitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
                    ;

                }

                Canvas canvas = new Canvas(mBitmap);

                FileOutputStream mFileOutStream = new FileOutputStream(mypath);
                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 100, mFileOutStream);
                Bitmap bitmapOrg = Media.getBitmap(getContentResolver(), Uri.fromFile(mypath));
                ByteArrayOutputStream hrhk = new ByteArrayOutputStream();
                bitmapOrg.compress(Bitmap.CompressFormat.PNG, 100, hrhk);
                HRHK = hrhk.toByteArray();

                bb = Base64.encode(HRHK);

                byte[] converttobyte = bb.getBytes();
                s1 = bb.replace("\n", "").replace("\r", "").replaceAll(" ", "");
                s2 = s1.replace("\n", "").replace("\r", "").replaceAll(" ", "");
                s3 = s2.replace("\n", "").replace("\r", "").replaceAll(" ", "");
                byte[] BA2 = Base64.decode(bb);
                hrhk.flush();
                hrhk.close();
                String url = Media.insertImage(getContentResolver(), mBitmap, "title", null);
            } catch (Exception e) {
//                Log.v("log_tag", e.toString());
                Crashlytics.log(android.util.Log.ERROR, TAG, "signature save" + e.getMessage());

            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "UnsatisfiedLinkError signature save" + err.getMessage());

            }
        }

        public void clear() {
            try {
                path.reset();
                invalidate();
            } catch (Exception e) {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "signature clear" + e.getMessage());
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "signature UnsatisfiedLinkError clear" + err.getMessage());

            }
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            try {
                float eventX = event.getX();
                float eventY = event.getY();

                mGetDelivered.setEnabled(true);
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        path.moveTo(eventX, eventY);
                        lastTouchX = eventX;
                        lastTouchY = eventY;
                        return true;

                    case MotionEvent.ACTION_MOVE:

                    case MotionEvent.ACTION_UP:

                        resetDirtyRect(eventX, eventY);
                        int historySize = event.getHistorySize();
                        for (int i = 0; i < historySize; i++) {
                            float historicalX = event.getHistoricalX(i);
                            float historicalY = event.getHistoricalY(i);
                            expandDirtyRect(historicalX, historicalY);
                            path.lineTo(historicalX, historicalY);
                        }
                        path.lineTo(eventX, eventY);
                        break;

                    default:
                        debug("Ignored touch event: " + event.toString());
                        return false;
                }

                invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                        (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

                lastTouchX = eventX;
                lastTouchY = eventY;

            } catch (Exception e) {
                e.getStackTrace();

            }
            return true;
        }

        private void debug(String string) {
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

    public void toggleMenu(View v) {
        finish();
    }

    /**
     * Listener Implementation of Spinner For Selecting Room
     */

    public class Listener_Of_Selecting_Room_Spinner implements OnItemSelectedListener {


        @SuppressLint("NewApi")
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            try {
                // By using this you can get the position of item which you
                // have selected from the dropdown

                RoomType = (parent.getItemAtPosition(pos)).toString();
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                if (!RoomType.equals("SELECT RELATIONSHIP")) {
                    received_by.setEnabled(true);
                    received_by.setFocusable(true);
//			   received_by.setFocusableInTouchMode(true);
                    received_by.requestFocus();
                    receiver_contact_num.setEnabled(true);
                    receiver_contact_num.setFocusable(true);

                    received_by.setText("");
                    receiver_contact_num.setText("");
                    /***code used to clear the signature*****/
                    if (RoomType.equals("SELF")) {
                        receiver_contact_num.setEnabled(true);
                        receiver_contact_num.setFocusable(true);
                        receiver_contact_num.setFocusableInTouchMode(true);
                        receiver_contact_num.requestFocus();
                        if (session_DB_PATH != null && session_DB_PWD != null) {
                            db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                            @SuppressWarnings("unused")
                            String shw[] = session_RESPONSE_AWB_NUM.split(",");
                            for (int i = 0; i < shw.length; i++) {
                                result = shw[i];
                                result = result.replace("/", "");
                                result = result.replace(" ", "");
                                result = result.replace("-", "");
                                result = result.replace(":", "");
//						   result = shw_s[i];
//		        			        	String strFilter = "T_Assignment_Number=" + result;
                                String strFilter = "T_Assignment_Number='" + result + "' ";
//					   String strFilter = "T_Assignment_Number=" + employeeId;
                                @SuppressWarnings("unused")
                                String isCOD = "";
                                c_chk = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount,emp.T_Signature FROM TOM_Assignments   emp LEFT OUTER JOIN TOM_Assignments   mgr ON emp._id = mgr._id  where emp.T_Assignment_Number ='" + result + "'", null);
//							   new String[]{"" + result});
                                while (c_chk.moveToNext()) {
                                    name = c_chk.getString(c_chk.getColumnIndex("T_Consignee_Name"));
//			   			Log.e(TAG,"Consignee name" + name);
                                    received_by.setText(name);

                                    /***code used to clear the signature*****/
                                    if (iv == null || iv.equals("null")) {
                                    } else if (iv != null) {
                                        iv.setImageBitmap(null);
                                        iv.destroyDrawingCache();
                                        sign1 = "null";
                                    }
                                }
                            }
                            c_chk.close();
                            db.close();
                        }
                    } else {
                        received_by.setText("");
                        received_by.setEnabled(true);
                        received_by.setFocusable(true);
                        received_by.setFocusableInTouchMode(true);
                        received_by.requestFocus();

                        receiver_contact_num.setText("");
                        receiver_contact_num.setEnabled(true);
                        /***code used to clear the signature*****/
                        if (iv == null || iv.equals("null")) {
                        } else if (iv != null) {
                            iv.setImageBitmap(null);
                            iv.destroyDrawingCache();
                            sign1 = "null";
                        }
                        sign1 = null;
                        theImage = null;
                        iv = null;
                    }

                } else {
                    s.setFocusable(true);
                    s.requestFocus();
                    received_by.setEnabled(true);
                    received_by.setFocusable(true);
                    received_by.requestFocus();
                    receiver_contact_num.setEnabled(true);
                    received_by.setText("");
                    receiver_contact_num.setText("");
                    sign1 = null;
                    theImage = null;
                    iv = null;
                }

            } catch (Exception e) {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Listener_Of_Selecting_Room_Spinner" + e.getMessage());

            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Listener_Of_Selecting_Room_Spinner UnsatisfiedLinkError" + err.getMessage());
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Do nothing.
        }

    }

    /**
     * FUNCTION TO CAPTURE IMAGE/PHOTO
     **/

    private void takePhoto() {
        try {
            new SoapAccessTask().execute();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "takePhoto " + e.getMessage());

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "takePhoto UnsatisfiedLinkError" + err.getMessage());

        }
    }


    private File getFile(Context context) {
        try {
//    		delete_older();
//	       final File path = new File( Environment.getExternalStorageDirectory(), "Image keeper" );	   
            final File path = new File(Environment.getExternalStorageDirectory()
                    + "/Android/data/com.traconmobi.in/", "Image keeper");
            photoname = null;
            if (!path.exists()) {
                path.mkdir();
            }
            try {
//	    	   File sdcard = Environment.getExternalStorageDirectory();
                File sdcard = new File(Environment.getExternalStorageDirectory()
                        + "/Android/data/com.traconmobi.in");
                File mymirFolder = new File(sdcard.getAbsolutePath() + "/Image keeper/");
                String shw[] = session_RESPONSE_AWB_NUM.split(",");
                for (int i = 0; i < shw.length; i++) {
                    result = shw[i];

                    result = result.replace("/", "");
                    result = result.replace(" ", "");
                    result = result.replace("-", "");
                    result = result.replace(":", "");
                    photoname = result + ".png";
                    Log.e(TAG, "print multi photoname" + photoname);
//			       photoname.replace("/", "");
                    fileimage = new File(path, photoname);
                }
                if (!mymirFolder.exists()) {
                    File noMedia = new File(mymirFolder.getAbsolutePath() + "/.nomedia");
                    noMedia.mkdirs();
                    noMedia.createNewFile();
                }
            } catch (IOException e) {

                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "getFile " + e.getMessage());
            //onClickGoToHomePage();
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "getFile UnsatisfiedLinkError" + err.getMessage());
            //onClickGoToHomePage();
            //        	    			finish();
        }
        return fileimage;


    }

    //deleting from /Android/data/com.traconmobi.in/Image keeper folder

    private void delete_older() {
        try {
            // TODO Auto-generated method stub
            File f = new File(Environment.getExternalStorageDirectory() + "/Android/data/com.traconmobi.in/Image keeper");

            //Log.i("Log", "file name in delete folder :  "+f.toString());
            File[] files = f.listFiles();

            Log.i("Log", "delete_older List of files is: " + files.toString());

            Log.e(TAG, "print date " + new Date().getDate());

            for (int i = 0; i <= files.length; i++) {
                long diff = new Date().getTime() - files[i].lastModified();
                int x = 3;

                long tme = x * 24 * 60 * 60 * 1000;


                if (diff > x * 24 * 60 * 60 * 1000) {
                    files[i].delete();
                }
            }
            Arrays.sort(files, new Comparator<Object>() {
                public int compare(Object o1, Object o2) {


                    if (((File) o1).lastModified() > ((File) o2).lastModified()) {
                        Log.i("Log", "Going -1");
                        return -1;
                    } else if (((File) o1).lastModified() < ((File) o2).lastModified()) {
                        Log.i("Log", "Going +1");
                        return 1;
                    } else {
                        Log.i("Log", "Going 0");
                        return 0;
                    }
                }

            });

            //Log.i("Log", "Count of the FILES AFTER DELETING ::"+files[0].length());
//        files[0].delete();
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "delete_older " + e.getMessage());

        }
    }

    //***********************************FUNCTION TO GET THE SIGNATURE OF THE RECIEVER**************************************//

    public void getsignature(View v) {
        try {
            Intent intent = new Intent(AssignedMultipleDelActivity.this, CaptureSignature.class);
//    	intent.putExtra(EXTRA_MESSAGE, employeeId);
            Log.e(TAG, "print session_RESPONSE_AWB_NUM" + session_RESPONSE_AWB_NUM);
            String shw[] = session_RESPONSE_AWB_NUM.split(",");
            intent.putExtra(EXTRA_MESSAGE1, session_RESPONSE_AWB_NUM);
            startActivityForResult(intent, SIGNATURE_ACTIVITY);
            mGetDelivered.setEnabled(true);
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "getsignature " + e.getMessage());

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "getsignature " + err.getMessage());

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
//    	super.onActivityResult(requestCode, resultCode, data);
            switch (requestCode) {
                //********************************Activity Result for Photo/Image **************************//
                case TAKE_PHOTO_CODE:

                    if (requestCode == TAKE_PHOTO_CODE) {
                        if (resultCode == RESULT_OK) {

                            try {
                                final Calendar c = Calendar.getInstance();
                                String month = c.get(Calendar.MONTH) + 1 + "";
                                if (month.length() < 2) {
                                    month = "0" + month;
                                }
                                String date = c.get(Calendar.DAY_OF_MONTH) + "";
                                if (date.length() < 2) {
                                    date = "0" + date;
                                }
                                photo_dt = "" + c.get(Calendar.YEAR) + "-" + month + "-" +
                                        date;
                                photo_tme = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
                                        (c.get(Calendar.MINUTE)) + ":" +
                                        (c.get(Calendar.SECOND));
                                Log.w("TIME:", String.valueOf(photo_tme) + currentTime);
                                if (session_DB_PATH != null && session_DB_PWD != null) {
                                    db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                    String shw[] = session_RESPONSE_AWB_NUM.split(",");
                                    for (int i = 0; i < shw.length; i++) {
                                        result = shw[i];

                                        result = result.replace("/", "");
                                        result = result.replace(" ", "");
                                        result = result.replace("-", "");
                                        result = result.replace(":", "");
                                        String strFilter = "T_Assignment_Number='" + result + "' ";
                                        ContentValues cv = new ContentValues();
                                        photoname = result + ".png";
                                        cv.put("T_Photo", photoname);
                                        db.update("TOM_Assignments", cv, strFilter, null);

                                    }
                                    db.close();
                                } else {
                                    Log.e(TAG, "session_DB_PATH is null ");
                                    //onClickGoToHomePage();
                                }

                                if (photoname != null && photoname != "" && !(photoname.equals("null"))) {
                                    image = new File(Environment.getExternalStorageDirectory()
                                            + "/Android/data/com.traconmobi.in/", "Image keeper/" + photoname.trim());
                                    selectedImagePath = Environment.getExternalStorageDirectory()
                                            + "/Android/data/com.traconmobi.in" + "/Image keeper/" + photoname.trim();
                                    Bitmap bm = reduceImageSize(selectedImagePath);

                                    if (bm != null) {

                                        ByteArrayOutputStream BAO = new ByteArrayOutputStream();

                                        bm.compress(Bitmap.CompressFormat.JPEG, 40, BAO);

                                        BAvalue = BAO.toByteArray();
                                        getphotobytearray = Base64.encode(BAvalue);
                                        s_photo = getphotobytearray.replace("+", "%2B");
//		    				    Log.e(TAG,"s_photo " + s_photo);
                                    } else if (bm == null) {
                                        getphotobytearray = "null";
                                    }
                                } else if (photoname == null && photoname == "" && photoname.equals("null")) {
                                    getphotobytearray = "null";
                                }


                                ByteArrayOutputStream bao = new ByteArrayOutputStream();

                                byte[] ba2 = Base64.decode(getphotobytearray);

                                ByteArrayInputStream imageStream_sign = new ByteArrayInputStream(ba2);
                                theImage = BitmapFactory.decodeStream(imageStream_sign);
                                iv = (ImageView) findViewById(R.id.photo);
                                iv.setImageBitmap(theImage);
                                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                                iv.setAdjustViewBounds(true);
                                Toast toast = Toast
                                        .makeText(this, "Photo capture Successfull", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.BOTTOM, 105, 50);
                                toast.show();
                            } catch (SQLiteException e) {
                                // TODO: handle exception
                                e.printStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + e.getMessage());

                            } catch (UnsatisfiedLinkError err) {
                                err.getStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + err.getMessage());

                            } catch (OutOfMemoryError o) {
                                o.getStackTrace();
                                Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + o.getMessage());

                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + e.getMessage());
                            } finally {
                                if (db != null) {
                                    db.close();
                                }
                            }
                            break;

                        } else if (resultCode == RESULT_CANCELED) {
                            photoname = "null";
//              Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                            Toast toast = Toast
                                    .makeText(this, "Picture was not taken", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 105, 50);
                            toast.show();

                        } else {
                            photoname = "null";
//              Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
                            Toast toast = Toast
                                    .makeText(this, "Picture was not taken", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 105, 50);
                            toast.show();

                        }
                    } else {
                        photoname = "null";
                        Toast toast = Toast
                                .makeText(this, "No Photo captured", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.BOTTOM, 105, 50);
                        toast.show();
                    }
                    //********************************Activity Result for signature **************************//
                case SIGNATURE_ACTIVITY:
                    if (resultCode == RESULT_OK) {

                        Bundle bundle = data.getExtras();
                        String status = bundle.getString("status");
                        @SuppressWarnings("unused")
                        String sign = bundle.getString("getsign");
                        sign1 = CaptureSignature.s1;

                        if (sign1 != null) {

                            if (status.equalsIgnoreCase("done")) {

                                @SuppressWarnings("unused")
                                ByteArrayOutputStream bao = new ByteArrayOutputStream();

                                byte[] ba2 = Base64.decode(sign1);

                                ByteArrayInputStream imageStream_sign = new ByteArrayInputStream(ba2);
                                theImage = BitmapFactory.decodeStream(imageStream_sign);
                                iv = (ImageView) findViewById(R.id.sign);
                                iv.setImageBitmap(theImage);
                                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                                iv.setAdjustViewBounds(true);
                                Toast toast = Toast.makeText(this, "Signature capture successful!", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.BOTTOM, 105, 50);
                                toast.show();
                                mGetDelivered.setEnabled(true);
                                mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
                            } else if (status.equalsIgnoreCase("cancel")) {
                                sign1 = null;
                                theImage = null;
                                iv = null;
                                iv = (ImageView) findViewById(R.id.sign);
                                iv.setImageBitmap(theImage);
                                iv.setScaleType(ImageView.ScaleType.FIT_XY);
                                iv.setAdjustViewBounds(true);

                                Toast toast = Toast.makeText(this, "Signature not captured !", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.BOTTOM, 105, 50);
                                toast.show();
                            }
                        } else // if(sign1 == null)
                        {
                            sign1 = null;
                            theImage = null;
                            iv = null;
                            iv = (ImageView) findViewById(R.id.sign);
                            iv.setImageBitmap(theImage);
                            iv.setScaleType(ImageView.ScaleType.FIT_XY);
                            iv.setAdjustViewBounds(true);

                            Toast toast = Toast
                                    .makeText(this, "Please capture Signature!", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.BOTTOM, 105, 50);
                            toast.show();
                        }

                        break;
                    }
            }
        } catch (Exception e) {
            e.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + e.getMessage());

        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "onActivityResult " + err.getMessage());
        }
    }

    public Bitmap reduceImageSize(String selectedImagePath) {
        Bitmap m = null;
        try {
            File f = new File(selectedImagePath);

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            //The new size we want to scale to
            final int REQUIRED_SIZE = 150;

            //Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            m = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Image File not found in your phone. Please select another image. " + e.getMessage());
//			  Toast.makeText(getApplicationContext(), "Image File not found in your phone. Please select another image.", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "reduceImageSize" + e.getMessage());
        }
        return m;
    }


    //starting asynchronus task
    class SoapAccessTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            //if you want, start progress dialog here
            // NOTE: You can call UI Element here.

        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                final Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                image = getFile(AssignedMultipleDelActivity.this);
                intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
                startActivityForResult(intent, TAKE_PHOTO_CODE);
            } catch (Exception e) {
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "SoapAccessTask" + e.getMessage());
            } catch (UnsatisfiedLinkError err) {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "SoapAccessTask UnsatisfiedLinkError" + err.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            // NOTE: You can call UI Element here.
            // Close progress dialog

        }
    }

    /**
     * METHOD TO SAVE COD DETAILS
     */
    public void oncodDialogButtonClick(View v) {
        try {
            final Calendar c = Calendar.getInstance();
            String month = c.get(Calendar.MONTH) + 1 + "";
            if (month.length() < 2) {
                month = "0" + month;
            }
            String date = c.get(Calendar.DAY_OF_MONTH) + "";
            if (date.length() < 2) {
                date = "0" + date;
            }
            showdate = "" + c.get(Calendar.YEAR) + "-" + month + "-" +
                    date;
            showtime = " " +
                    c.get(Calendar.HOUR_OF_DAY) + ":" +
                    c.get(Calendar.MINUTE) + ":" +
                    c.get(Calendar.SECOND);

            currentTime = (c.get(Calendar.HOUR_OF_DAY)) + ":" +
                    (c.get(Calendar.MINUTE)) + ":" +
                    (c.get(Calendar.SECOND));


            /** Get the values provided by the user via the UI **/

            received_by = (EditText) findViewById(R.id.txtRecvBy);
            Receiver = received_by.getText().toString().trim();
            receiver_contact_num = (EditText) findViewById(R.id.txtcontct_no);
            Receiver_num = receiver_contact_num.getText().toString().trim();

            st = s.getSelectedItem().toString();

            if (st.equals("Select") || st.equals("SELECT RELATIONSHIP")) {
                Toast toast = Toast.makeText(AssignedMultipleDelActivity.this,
                        "Please Select the Relationship type !!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                s.setFocusable(true);
//				 s.setFocusableInTouchMode(true);
                s.requestFocus();
//				 received_by.setEnabled(false);
                mGetDelivered.setEnabled(true);
//		mGetDelivered.setBackgroundResource(R.drawable.button);
                mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
            } else if (Receiver.isEmpty()) {
                Toast toast = Toast.makeText(AssignedMultipleDelActivity.this,
                        "Please enter receiver name !!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                mGetDelivered.setEnabled(true);
//			 mGetDelivered.setBackgroundResource(R.drawable.button);
                mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
            } else if (Receiver_num.isEmpty()) {
                Toast toast = Toast.makeText(AssignedMultipleDelActivity.this,
                        "Please enter receiver contact number !!", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                mGetDelivered.setEnabled(true);
                mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
            } else {

                if (Receiver.toString() != null && Receiver_num.toString() != null && st.equals("SELECT RELATIONSHIP")) {

                    Toast.makeText(AssignedMultipleDelActivity.this,
                            "Please Select the Relationship type !!", Toast.LENGTH_LONG).show();

                    mGetDelivered = (Button) findViewById(R.id.btnsave);
                    Toast toast = Toast.makeText(AssignedMultipleDelActivity.this,
                            "Please capture signature !!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    mGetDelivered.setEnabled(false);
                    return;

                }
                if (Receiver.toString() != null && Receiver_num.toString() != null && !st.equals("SELECT RELATIONSHIP")) {
                    mView.setDrawingCacheEnabled(true);
//		mSignature.save(mView);	
                    if (Double.parseDouble(session_COD_AMT) == 0) //isCOD.equals("0") &&
                    {
                        if (sign1 == null || sign1.isEmpty() || sign1.equals("null")) {
                            Toast toast = Toast
                                    .makeText(this, "Please capture Signature!", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            Log.e(TAG, "sign1 " + sign1);
                        } else if (sign1 != null || !(sign1.isEmpty()) || !(sign1.equals("null"))) {
                            mGetDelivered.setEnabled(true);
//         		mGetDelivered.setBackgroundResource(R.drawable.button);
                            mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

                            // Setting Dialog Title
                            alertDialog.setTitle("Alert");

                            // Setting Dialog Message
                            alertDialog.setMessage("Are you sure you want to confirm ?");

                            // On pressing Settings button
                            alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        if (listener != null) {
                                            listener.onSave();
                                        }

                                    } catch (Exception e) {
                                        Crashlytics.log(android.util.Log.ERROR, TAG, "podupdate" + e.getMessage());
                                    } finally {

                                    }

                                }
                            });

                            // on pressing cancel button
                            alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
//	            	Intent goToNextActivity = new Intent(getApplicationContext(),TOMDel_pickup_screenActivity.class);
//	    			 startActivity(goToNextActivity);
                                    mGetDelivered.setEnabled(true);
//						 mGetDelivered.setBackgroundResource(R.drawable.button);
                                    mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
                                    dialog.cancel();
                                }
                            });

                            // Showing Alert Message
                            alertDialog.show();
                        }
                    } else if (Double.parseDouble(session_COD_AMT) > 0) //isCOD.equals("1") &&
                    {

                        if (sign1 == null || sign1.isEmpty() || sign1.equals("null")) {
                            Toast toast = Toast
                                    .makeText(this, "Please capture Signature!", Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            Log.e(TAG, "sign1 true amount insde null " + sign1);
                        } else if (sign1 != null || !(sign1.isEmpty()) || !(sign1.equals("null"))) {

                            startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                            Log.e(TAG, "sign1 true amount insde not null" + sign1);
                            NumberFormat formatter = new DecimalFormat("###.00");
                            str_amt = formatter.format(Double.parseDouble(session_COD_AMT));
                            Log.e(TAG, "get the decimal points with value" + str_amt);


                            int width = 300;
                            int height = 100;
                            LinearLayout layout = new LinearLayout(this);
                            layout.setOrientation(LinearLayout.VERTICAL);
                            layout.setLayoutParams(new LayoutParams(width, height));
//		    	layout.addView(layout, width, height);


                            final TextView tv1 = new TextView(this);
                            tv1.setText("COD Alert");
                            tv1.setTextColor(Color.BLACK);
                            tv1.setTypeface(tv1.getTypeface(), Typeface.BOLD);
                            tv1.setGravity(Gravity.CENTER_HORIZONTAL);
//					 textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
                            layout.addView(tv1);

                            final TextView tv2 = new TextView(this);
                            tv2.setText("This purschase is on COD.Make sure you received the full amount.Please write COD amount to continue.COD amount is Rs." + str_amt);
                            tv2.setTextColor(Color.BLACK);
                            tv2.setGravity(Gravity.CENTER_HORIZONTAL);
                            layout.addView(tv2);

                            /** Set an EditText view to get user input  */
                            final EditText input = new EditText(this);
                            input.getInputType();
                            input.setSingleLine();
                            //setting text field limit to 15
                            int maxLength = 15;
                            input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
                            input.setInputType(InputType.TYPE_CLASS_NUMBER);
                            input.setKeyListener(DigitsKeyListener.getInstance("0123456789."));
                            layout.addView(input);

                            final TextView tv3 = new TextView(this);
                            tv3.setText("");
                            tv3.setTextColor(Color.BLACK);
                            layout.addView(tv3);

                            btn = new Button(this);
                            btn.setEnabled(true);
//	            	btn.setBackgroundResource(R.drawable.button);
                            btn.setBackgroundResource(R.drawable.button_bgdark_stroke);

                            btn.setText("Confirm");
                            btn.setMaxWidth(5);
                            btn.setTextColor(Color.WHITE);
                            layout.addView(btn);
                            btn.setOnClickListener(new OnClickListener() {
                                                       @Override
                                                       public void onClick(View v) {
                                                           btn.setEnabled(false);
//                   		 	btn.setBackgroundColor(Color.GRAY);
                                                           if (input.getText().toString().equals("")) {
                                                               tv3.setText("Enter amount collected");
                                                               btn.setEnabled(true);
                                                           } else if (Double.parseDouble(session_COD_AMT) > 0) {
                                                               startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                                                               if (Double.parseDouble(input.getText().toString()) == Double.parseDouble(session_COD_AMT))  //if(input.getText().toString().equals(str_amt) || input.getText().toString().equals(COD_Amount))
                                                               {
                                                                   AlertDialog.Builder alertDialog = new AlertDialog.Builder(AssignedMultipleDelActivity.this);

                                                                   // Setting Dialog Title
                                                                   alertDialog.setTitle("Alert");

                                                                   // Setting Dialog Message
                                                                   alertDialog.setMessage("Are you sure you want to confirm ?");

                                                                   // On pressing Settings button
                                                                   alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                                                       @Override
                                                                       public void onClick(DialogInterface dialog, int which) {
                                                                           try {
                                                                               if (session_DB_PATH != null && session_DB_PWD != null) {
                                                                                   if (listener1 != null) {
                                                                                       listener1.saveCODDetails();
                                                                                   }

                                                                               }
                                                                               dialog.dismiss();
                                                                           } catch (SQLException s) {
                                                                               Log.e(TAG, "Exception db path SQLException " + s.getMessage());
                                                                           } catch (Exception e) {
                                                                               Log.e(TAG, "Exception db path  " + e.getMessage());
                                                                           } finally {

                                                                           }
                                                                       }
                                                                   });

                                                                   // on pressing cancel button
                                                                   alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                                                       @Override
                                                                       public void onClick(DialogInterface dialog, int which) {
//	         		            	Intent goToNextActivity = new Intent(getApplicationContext(),TOMDel_pickup_screenActivity.class);
//	         		    			 startActivity(goToNextActivity);
                                                                           btn.setEnabled(true);
//																   btn.setBackgroundResource(R.drawable.button);
                                                                           mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
                                                                           dialog.cancel();
                                                                       }
                                                                   });

                                                                   // Showing Alert Message
                                                                   alertDialog.create().show();
                                                                   /****************************************************************************/


                                                               } else {
                                                                   tv3.setText("Cannot exceed or decrease amount : " + str_amt);
                                                                   btn.setEnabled(true);
//		                		btn.setBackgroundResource(R.drawable.button);
//		                		btn.setMaxWidth(10);	
                                                               }
                                                           }

                                                       }

                                                   }
                            );

                            final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                            mGetDelivered.setEnabled(true);
//					 mGetDelivered.setBackgroundResource(R.drawable.button);
                            mGetDelivered.setBackgroundResource(R.drawable.button_bgdark_stroke);
                            alert.setView(layout);
                            alert.create();
                            alert.setCancelable(true);
                            alert.show();

                        }
                    }

                }

            }
        } catch (SQLiteException e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "pod update SQLiteException" + e.getMessage());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "pod update" + e.getMessage());
        } catch (UnsatisfiedLinkError err) {
            err.getStackTrace();
            Crashlytics.log(android.util.Log.ERROR, TAG, "pod update UnsatisfiedLinkError" + err.getMessage());
        } finally {
            if (crs != null) {
                crs.close();
            }

            if (cursor != null) {
                cursor.close();
            }

            if (db != null) {
                db.close();
            }
        }

    }

    /*  @Override
      public void onBackPressed() {
      }
  */
    @Override
    public void onDestroy() {
       super.onDestroy();
    }

    private class CursorTask extends AsyncTask<Void, Void, Void> {
        final ProgressDialog dialog = new ProgressDialog(AssignedMultipleDelActivity.this);


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait for Report Building...");
            this.dialog.show();
            this.dialog.setCancelable(true);
        }


        CursorTask() {
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                if (session_DB_PATH != null && session_DB_PWD != null) {
                    if (SESSION_TRANSPORT.equals("BIKER")) {
                        startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                        if (session_DB_PATH != null && session_DB_PWD != null) {
                            db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                            String shw_s[] = session_RESPONSE_AWB_NUM.split(",");
                            for (int i = 0; i < shw_s.length; i++) {
                                result = shw_s[i];
                                String strFilter = "T_Assignment_Number='" + result + "' ";
                                crs = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount,mgr.T_IMEI FROM TOM_Assignments   emp JOIN TOM_Users  mgr ON mgr.T_U_ID= emp.T_OUT_Scan_U_ID  where emp.T_Assignment_Number='" + result + "'", null);
                                while (crs.moveToNext()) {
                                    imei_num = crs.getString(crs.getColumnIndex("T_IMEI"));
                                    ContentValues cv = new ContentValues();
                                    cv.put("B_is_Completed", "TRUE");
                                    ;
                                    cv.put("F_Amount_Collected", "0");
                                    cv.put("T_Received_by_Collected_from", Receiver);
                                    cv.put("T_Receiver_Contact_Num", Receiver_num);
                                    cv.put("T_Relationship", st);
                                    cv.put("D_Completed_Date", showdate);
                                    cv.put("T_AcquiredLat", new_lat);
                                    cv.put("T_AccquiredLon", new_lon);
                                    cv.put("D_Completed_time", String.valueOf(currentTime));
                                    cv.put("T_Signature", sign1);
                                    String sign_dt_tm = CaptureSignature.s1_datetime;
                                    cv.put("T_Signature_dt", sign_dt_tm);

                                    if (clicked == false) {
                                        photoname = "null";
                                    }
                                    cv.put("T_Photo", photoname);
                                    cv.put("T_Photo_dt", photo_dt);
                                    cv.put("T_Photo_tm", String.valueOf(photo_tme));
                                    cv.put("C_is_Sync", "0");
                                    db.update("TOM_Assignments", cv, strFilter, null);
                                }
                                crs.close();
                            }
                            db.close();

                        } else {
                            Log.e(TAG, "Error TOMCODUpdateActivity step 6 TOMLoginUserActivity.file is null ");
                            //onClickGoToHomePage();
                        }
                    } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {

                        startService(new Intent(getApplicationContext(), PowerConnectionReceiver.class));
                        if (session_DB_PATH != null && session_DB_PWD != null) {
//		        	            	db=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                            db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                            String shw_s[] = session_RESPONSE_AWB_NUM.split(",");
                            for (int i = 0; i < shw_s.length; i++) {
                                result = shw_s[i];
//		        			        	String strFilter = "T_Assignment_Number=" + result;
                                String strFilter = "T_Assignment_Number='" + result + "' ";
//		        	            	crs = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount,mgr.T_IMEI FROM TOM_Assignments   emp JOIN TOM_Users  mgr ON mgr.T_U_ID= emp.T_OUT_Scan_U_ID  where emp.T_Assignment_Number = ? and emp.D_OutScan_Date='" + session_CURRENT_DT +"'",
                                crs = db.rawQuery("SELECT emp._id,emp .T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount,mgr.T_IMEI FROM TOM_Assignments   emp JOIN TOM_Users  mgr ON mgr.T_U_ID= emp.T_OUT_Scan_U_ID  where emp.T_Assignment_Number='" + result + "'", null);
//		        	        			new String[]{""+result});
                                while (crs.moveToNext()) {
//									room = Listener_Of_Selecting_Room_Spinner.RoomType;
//									room.toString();
//									Log.e(TAG,"RoomType" + room.toString());
                                    imei_num = crs.getString(crs.getColumnIndex("T_IMEI"));
                                    ContentValues cv = new ContentValues();
                                    cv.put("B_is_Completed", "TRUE");
                                    ;
                                    cv.put("F_Amount_Collected", "0");
                                    cv.put("T_Received_by_Collected_from", Receiver);
                                    cv.put("T_Receiver_Contact_Num", Receiver_num);
                                    cv.put("T_Relationship", st);
                                    cv.put("D_Completed_Date", showdate);
                                    cv.put("T_AcquiredLat", new_lat);
                                    cv.put("T_AccquiredLon", new_lon);
                                    cv.put("D_Completed_time", String.valueOf(currentTime));
                                    cv.put("T_Signature", sign1);
                                    String sign_dt_tm = CaptureSignature.s1_datetime;
                                    cv.put("T_Signature_dt", sign_dt_tm);
                                    if (clicked == false) {
                                        photoname = "null";
                                    }
                                    cv.put("T_Photo", photoname);
                                    cv.put("T_Photo_dt", photo_dt);
                                    cv.put("T_Photo_tm", String.valueOf(photo_tme));
                                    cv.put("C_is_Sync", "0");
                                    db.update("TOM_Assignments", cv, strFilter, null);
                                    Log.e(TAG, "cv " + cv);
                                }
                                crs.close();
                            }
                            db.close();
                        } else {
                            Log.e(TAG, "Error TOMCODUpdateActivity step 6 TOMLoginUserActivity.file is null ");
                            //onClickGoToHomePage();
                        }


                    }


                    //}
                } else {
                    Log.e(TAG, "session db path is null ");
//				onClickGoToHomePage();
                }

            } catch (OutOfMemoryError e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (SQLException e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                db.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void reult) {

            if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_MULTIPLE_DEL")) {
                Toast.makeText(AssignedMultipleDelActivity.this, "Record Saved succesfully", Toast.LENGTH_LONG).show();
                Intent goToNextActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
                startActivity(goToNextActivity);
            }
            if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_MULTIPLE_UNDEL")) {
                Toast.makeText(AssignedMultipleDelActivity.this, "Record Saved succesfully", Toast.LENGTH_LONG).show();
                Intent goToNextActivity = new Intent(getApplicationContext(), AssignmentInCompleteStatus.class);
                startActivity(goToNextActivity);
            }

            mGetDelivered.setEnabled(false);
            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

        }
    }

    private class DelSavedTask extends AsyncTask<Void, Void, Void> {
        final ProgressDialog dialog = new ProgressDialog(AssignedMultipleDelActivity.this);


        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait for Report Building...");
            this.dialog.show();
            this.dialog.setCancelable(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (session_DB_PATH != null && session_DB_PWD != null) {
                    if (SESSION_TRANSPORT.equals("BIKER")) {
                        try {

                            if (session_DB_PATH != null && session_DB_PWD != null) {
                                db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                String shw[] = session_RESPONSE_AWB_NUM.split(",");
                                for (int i = 0; i < shw.length; i++) {
                                    result = shw[i];
                                    String strFilter = "T_Assignment_Number='" + result + "' ";
                                    cursor = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount FROM TOM_Assignments   emp LEFT OUTER JOIN TOM_Assignments   mgr ON emp._id = mgr._id  where emp.T_Assignment_Number=='" + result + "' ", null);
                                    while (cursor.moveToNext()) {
                                        COD_Amount = cursor.getString(cursor.getColumnIndex("F_Amount"));
                                        ContentValues cv = new ContentValues();
                                        cv.put("B_is_Completed", "TRUE");
                                        cv.put("T_Received_by_Collected_from", Receiver);
                                        cv.put("T_Receiver_Contact_Num", Receiver_num);
                                        cv.put("T_Relationship", st);
                                        cv.put("D_Completed_Date", showdate);
                                        cv.put("T_AcquiredLat", new_lat);
                                        cv.put("T_AccquiredLon", new_lon);
                                        cv.put("D_Completed_time", String.valueOf(currentTime));
                                        cv.put("T_Signature", sign1);
                                        String sign_dt_tm = CaptureSignature.s1_datetime;
                                        cv.put("T_Signature_dt", sign_dt_tm);
                                        Log.e(TAG, "return click of button 3 " + clicked);
                                        if (clicked == false) {
                                            photoname = "null";
                                        }

                                        cv.put("T_Photo", photoname);
                                        cv.put("T_Photo_dt", photo_dt);
                                        cv.put("T_Photo_tm", String.valueOf(photo_tme));
                                        cv.put("C_is_Sync", "0");
                                        cv.put("F_Amount_Collected", COD_Amount);
                                        db.update("TOM_Assignments", cv, strFilter, null);
                                    }
                                    cursor.close();
                                }
                                db.close();

                            } else {
                                Log.e(TAG, "Error TOMCODUpdateActivity step 8 TOMLoginUserActivity.file is null ");

                            }

                        } catch (Exception e) {
                            Log.e(TAG, "Exception e" + e.getMessage());
                        }
                    } else if (SESSION_TRANSPORT.equals("NON-BIKER")) {
                        try {

                            if (session_DB_PATH != null && session_DB_PWD != null) {
                                db = SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.OPEN_READWRITE);
                                //		    	 		            	final String employeeId = getIntent().getStringExtra(AssignedDelMainActivity.EXTRA_MESSAGE );
                                String shw[] = session_RESPONSE_AWB_NUM.split(",");
                                for (int i = 0; i < shw.length; i++) {
                                    result = shw[i];
                                    String strFilter = "T_Assignment_Number='" + result + "' ";
                                    cursor = db.rawQuery("SELECT emp._id,emp.T_Assignment_Number, emp .T_Consignee_Name , emp .T_Address_Line1, emp .T_Address_Line2, emp .T_City , emp .T_Contact_number , emp .T_Pincode,emp.B_is_Amountable,emp.F_Amount FROM TOM_Assignments   emp LEFT OUTER JOIN TOM_Assignments   mgr ON emp._id = mgr._id  where emp.T_Assignment_Number=='" + result + "' ", null);
                                    while (cursor.moveToNext()) {
                                        COD_Amount = cursor.getString(cursor.getColumnIndex("F_Amount"));
                                        ContentValues cv = new ContentValues();
                                        cv.put("B_is_Completed", "TRUE");
                                        cv.put("T_Received_by_Collected_from", Receiver);
                                        cv.put("T_Receiver_Contact_Num", Receiver_num);
                                        cv.put("T_Relationship", st);
                                        cv.put("D_Completed_Date", showdate);
                                        cv.put("T_AcquiredLat", new_lat);
                                        cv.put("T_AccquiredLon", new_lon);
                                        cv.put("D_Completed_time", String.valueOf(currentTime));
                                        cv.put("T_Signature", sign1);
                                        String sign_dt_tm = CaptureSignature.s1_datetime;
                                        cv.put("T_Signature_dt", sign_dt_tm);
                                        Log.e(TAG, "return click of button 3 " + clicked);
                                        if (clicked == false) {
                                            photoname = "null";
                                        }

                                        cv.put("T_Photo", photoname);
                                        cv.put("T_Photo_dt", photo_dt);
                                        cv.put("T_Photo_tm", String.valueOf(photo_tme));
//         					 	 		        			cv.put("B_is_Completed", "1");
                                        cv.put("C_is_Sync", "0");
                                        cv.put("F_Amount_Collected", COD_Amount);
                                        db.update("TOM_Assignments", cv, strFilter, null);
                                        Log.e(TAG, "cv TOM_Assignments" + cv);
                                        //	 		        			db.close();
                                    }
                                    cursor.close();
                                }
                                db.close();

                            } else {
                                Log.e(TAG, "Error TOMCODUpdateActivity step 8 TOMLoginUserActivity.file is null ");

                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Exception e" + e.getMessage());
                        }
                    }


                } else {
                    Log.e(TAG, "Error AssignedMultipleDelActivity TOMLoginUserActivity.file is null ");
//	         					onClickGoToHomePage();
                }


            } catch (OutOfMemoryError e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (SQLException e) {
                Log.e(TAG, e.getMessage());
                db.close();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                db.close();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void reult) {
            btn.setEnabled(false);
            btn.setBackgroundColor(Color.GRAY);

            if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_MULTIPLE_DEL")) {
                Toast.makeText(AssignedMultipleDelActivity.this, "Record Saved succesfully", Toast.LENGTH_LONG).show();
                Intent goToNextActivity = new Intent(getApplicationContext(), AssignedListMainActivity.class);
                startActivity(goToNextActivity);
            }
            if (FORM_TYPE_VAL != null && FORM_TYPE_VAL != "" && FORM_TYPE_VAL.equals("FORM_MULTIPLE_UNDEL")) {
                Toast.makeText(AssignedMultipleDelActivity.this, "Record Saved succesfully", Toast.LENGTH_LONG).show();
                Intent goToNextActivity = new Intent(getApplicationContext(), AssignmentInCompleteStatus.class);
                startActivity(goToNextActivity);
            }

            if (this.dialog.isShowing()) {
                this.dialog.dismiss();
            }

        }
    }
}
