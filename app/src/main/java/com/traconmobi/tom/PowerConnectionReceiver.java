package com.traconmobi.tom;


import java.util.HashMap;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.database.sqlite.SQLiteException;
//import net.sqlcipher.database.DatabaseObjectNotClosedException;
//import android.database.sqlite.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteDatabase;
//import net.sqlcipher.database.SQLiteException;
import android.os.BatteryManager;

import android.os.IBinder;
import android.app.Service;

import android.widget.TextView;

import com.couchbase.lite.util.Log;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class PowerConnectionReceiver extends Service {
    TextView mTextView;
    protected SQLiteDatabase db_opn;
    Cursor cr_chkusr;

    BatteryReceiver mArrow;
    public String session_user_id,session_user_pwd,session_USER_LOC,session_USER_NUMERIC_ID,session_CURRENT_DT,session_CUST_ACC_CODE,session_USER_NAME,gettomnoti_response,session_noti_resp,session_outscan_resp,getoutscan_response,session_DB_PATH,session_DB_PWD;

    // Session Manager Class
    SessionManager session;
    public String TAG="PowerConnectionReceiver";
    public static int level;

    @Override
    public void onCreate() {

        try {
            //Intializing Fabric
            Fabric.with(this, new Crashlytics());
            this.registerReceiver(this.mArrow, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        new Thread(new ThreadM()).start();


    }
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Log.e("PowerConnectionReceiver ", "not null");
        try{
            if(mArrow!=null){
                Log.e("PowerConnectionReceiver ", "not null");
                unregisterReceiver(mArrow);
            }

        }catch(Exception e)
        {
        }
    }

    @Override
    public void onStart(Intent intent, int startId){
        new Thread(new ThreadM()).start();

    }

    public class ThreadM implements Runnable {
        @Override
        public void run()
        {
            try
            {
                mArrow = new BatteryReceiver();
                IntentFilter mIntentFilter = new IntentFilter();
                mIntentFilter.addAction(Intent.ACTION_BATTERY_LOW);
                mIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
                mIntentFilter.addAction(Intent.ACTION_BATTERY_OKAY);
                Intent batteryIntent = registerReceiver(mArrow, mIntentFilter);
                batteryLevel = getBatteryLevel(batteryIntent);
            }
            catch(Exception e)
            {
                unregisterReceiver(mArrow);
                e.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception PowerConnectionReceiver " + e.getMessage());
            }
        }
    }
    float batteryLevel;
    public class BatteryReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context arg0, Intent arg1)
        {
            try
            {
                if (arg1.getAction().equalsIgnoreCase(Intent.ACTION_BATTERY_LOW)
                        || arg1.getAction().equalsIgnoreCase(
                        Intent.ACTION_BATTERY_CHANGED)
                        || arg1.getAction().equalsIgnoreCase(
                        Intent.ACTION_BATTERY_OKAY)) {
                    level = arg1.getIntExtra("level", 0);


                    // Session class instance
                    session = new SessionManager(getApplicationContext());

                    /**GETTING SESSION VALUES**/

                    // get AuthenticateDb data from session
                    HashMap<String, String> authenticate_db_Dts = session.getAuthenticateDbDetails();

                    // DB_PATH
                    session_DB_PATH = authenticate_db_Dts.get(SessionManager.KEY_PATH);

                    // DB_PWD
                    session_DB_PWD = authenticate_db_Dts.get(SessionManager.KEY_DB_PWD);

                    // get user data from session
                    HashMap<String, String> login_Dts = session.getLoginDetails();

                    // Userid
                    session_user_id = login_Dts.get(SessionManager.KEY_UID);

                    // pwd
                    session_user_pwd = login_Dts.get(SessionManager.KEY_PWD);


                    // get user data from session
                    HashMap<String, String> user = session.getUserDetails();

                    // session_USER_LOC
                    session_USER_LOC= user.get(SessionManager.KEY_USER_LOC);

                    // session_USER_NUMERIC_ID
                    session_USER_NUMERIC_ID = user.get(SessionManager.KEY_USER_NUMERIC_ID);

                    // session_CURRENT_DT
                    session_CURRENT_DT= user.get(SessionManager.KEY_CURRENT_DT);

                    // session_CUST_ACC_CODE
                    session_CUST_ACC_CODE= user.get(SessionManager.KEY_CUST_ACC_CODE);

                    // session_USER_NAME
                    session_USER_NAME= user.get(SessionManager.KEY_USER_NAME);

                    String strFilter = "T_username='"+ session_USER_NAME +"'";
                    ContentValues cv = new ContentValues();
                    cv.put("T_Battery_usage",level);
                    if(session_DB_PATH != null && session_DB_PWD != null)
                    {
//                 			db_opn=SQLiteDatabase.openOrCreateDatabase(session_DB_PATH, session_DB_PWD, null);
                        db_opn=SQLiteDatabase.openDatabase(session_DB_PATH, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READWRITE);
//                 			db_opn.beginTransaction();
//                 			db_opn.updateWithOnConflict("TOM_Users",cv,strFilter, null, SQLiteDatabase.CONFLICT_REPLACE);
                        Cursor c=db_opn.rawQuery("UPDATE TOM_Users SET T_Battery_usage='"+ level +"' WHERE T_username ='"+ session_USER_NAME + "'", null);
                        while(c.moveToNext())
                        {

                        }
                        if (c != null)
                        {
                            c.close();
                        }
//                 			db_opn.setTransactionSuccessful();
//        			    	db_opn.endTransaction();
                        db_opn.close();
                    }
                    else
                    {
                        Crashlytics.log(android.util.Log.ERROR, TAG, "Error Battery usage TOMLoginUserActivity.file is null ");
                    }

                }
            }
            catch (SQLiteException e) {
                // TODO: handle exception
                e.printStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Battery usage " + e.getMessage());
            }
           catch(Exception e)
            {
                e.printStackTrace();

                //if(e.getMessage().toString().equals("unknown error (code 14): Could not open database"))
                if(e.getMessage().toString().equals("unknown error (code 14): Could not open database") || e.getMessage().toString().equals("unable to open database file"))

                {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Battery " + e.getMessage());
                }

                else
                {
                    Crashlytics.log(android.util.Log.ERROR, TAG, "Exception else Battery " + e.getMessage());
                    //              				Toast.makeText(PowerConnectionReceiver.this,"Please try again !!! " , Toast.LENGTH_LONG).show();
                }
            }
            catch(UnsatisfiedLinkError err)
            {
                err.getStackTrace();
                Crashlytics.log(android.util.Log.ERROR, TAG, "Error Battery UnsatisfiedLinkError ");

            }
            finally {

//              			if (cr_chkusr != null)
//              			{
//              				cr_chkusr.close();
//              			}
//              			if (db_opn != null && db_opn.isOpen())
//              			{
//              				db_opn.close();
//              			}
                if (db_opn != null)
                {
                    db_opn.close();
                }
            }


        }
    }
    //     }
    public float getBatteryLevel(Intent batteryIntent)
    {
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        if (level == -1 || scale == -1) {
            return 50.0f;
        }
        return ((float) level / (float) scale) * 100.0f;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        onStart(intent, startId);
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
}  