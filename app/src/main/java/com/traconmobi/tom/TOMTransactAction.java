/*************************************************************/

/**********CREATED BY : ASHWINI NAIK ************************/
/********CREATED DATE : 08/03/2013 **************************/
/***********PURPOSE   : TO SHOW THE DELIVERIES IN THE LISTVIEW
  						AND PERFORM OPERATIONS ON LISTVIEW ******/

/************************************************************/

package com.traconmobi.tom;

import android.widget.Button;


public class TOMTransactAction {
	private String label;
	private String data;

	private int type;

	public static final int ACTION_CALL = 1; 
	public static final int ACTION_SMS = 2; 
	public static final int ACTION_EMAIL = 3; 
	public static final int ACTION_VIEW = 4; 
	public static final int ACTION_REPORTS = 5; 
	public static final int btn_CALL = 6; 
	public TOMTransactAction(String label, String data, int type) {
		super();
		this.label = label;
		this.data = data;
		this.type = type;
	}


	public TOMTransactAction(String label2, Button button, int actionEmail) {
		// TODO Auto-generated constructor stub
		button.setText("hi");
	}


	public String getLabel() {
		return label;
	}


	public void setLabel(String label) {
		this.label = label;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	}

