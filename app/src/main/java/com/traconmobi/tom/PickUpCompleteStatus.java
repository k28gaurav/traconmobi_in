package com.traconmobi.tom;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import io.fabric.sdk.android.Fabric;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link //OnFragmentInteractionListener}
 * interface.
 */
public class PickUpCompleteStatus extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView mRecyclerView;
    private PickUpCompleteStatusAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private CouchBaseDBHelper cDB;
    ArrayList<PickUpCustomData> dataAdapter  = new ArrayList<PickUpCustomData>();;
    SharedPreferences pref;
    SessionManager session;
    TinyDB tiny;
    public String SESSION_TRANSPORT;
    public String TAG="PickUpCompleteStatus";
    final ArrayList<String> assign_no = new ArrayList<>();
    final ArrayList<String> assign_id = new ArrayList<>();
    final ArrayList<String> companyId = new ArrayList<>();
    final ArrayList<PickUpCustomData> addressAdapter = new ArrayList<PickUpCustomData>();
    ArrayList<String> incompleteList = new ArrayList<String>();
    ArrayList<String> addressinfo = new ArrayList<>();
    Date curDate = new Date();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    final String mLastUpdateTime = dateFormat.format(curDate).toString();
    Object[] dataId;
    OnItemTouchListener itemTouchListener;
    Set<String> setPickUp;
    //private OnFragmentInteractionListener mListener;
//    public static final String PREF_NAME = "ScanPref";


    // TODO: Rename and change types of parameters
    public static PickUpCompleteStatus newInstance(String param1, String param2) {
        PickUpCompleteStatus fragment = new PickUpCompleteStatus();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PickUpCompleteStatus() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Intializing Fabric
        Fabric.with(getActivity(), new Crashlytics());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_pick_up_incomplete_status, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.incompletelist);
        //mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        session = new SessionManager(getActivity().getApplicationContext());
        tiny = new TinyDB(getActivity().getApplicationContext());
        SESSION_TRANSPORT = tiny.getString("transport_val");
        try {
            pref = getActivity().getApplicationContext().getSharedPreferences(ParsePickupOutscanXML.PREF_NAME, Context.MODE_PRIVATE);
            setPickUp = session.getPickUpId(session.getUserDetails().get(session.KEY_USER_NAME));

            if (setPickUp != null) {
                itemTouchListener = new OnItemTouchListener() {
                    @Override
                    public void onCardViewTap(View view, int position) {
                        if (SESSION_TRANSPORT.equals("SELECT TRANSPORT") || SESSION_TRANSPORT.equals("")) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

                            // Setting Dialog Title
                            alertDialog.setTitle("Alert");

                            // Setting Dialog Message
                            alertDialog.setMessage("Please start the trip");
                            alertDialog.setCancelable(false);
                            // On pressing Settings button
                            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Intent goToNextActivity = new Intent(getActivity().getApplicationContext(), Start_End_TripMainActivity.class);
                                    goToNextActivity.putExtra("NAV_FORM_TYPE_VAL", "FORM_COMPLETE_PICKUP_SCAN");
                                    startActivity(goToNextActivity);
                                }
                            });
                            alertDialog.show();
                        }
                        else {
                            try {
                                //Toast.makeText(getActivity().getApplicationContext(), "Tapped " +dataAdapter.get(position), Toast.LENGTH_SHORT).show();
                                if(session.getAssignType(dataAdapter.get(position).getmAssignNo()).equalsIgnoreCase("RVP")) {
                                   // Do nothing
                                }
                                else if(session.getAssignType(dataAdapter.get(position).getmAssignNo()).equalsIgnoreCase("O")) {
                                    Intent i = new Intent(getActivity().getApplicationContext(), ECOMUserActivity.class);
                                    i.putExtra("NAV_FORM_TYPE_VAL", "FORM_PICKUP_COMPLETE");
                                    i.putExtra("pickId", dataAdapter.get(position).getmAssignNo());
                                    i.putExtra("assignId", dataAdapter.get(position).getmAssignId());
                                    i.putExtra("companyId", dataAdapter.get(position).getmCompanyId());
                                    //i.putStringArrayListExtra("listId", list);
                                    startActivity(i);
                                }
                                else {
                                    //Toast.makeText(getActivity().getApplicationContext(), "Tapped " +dataAdapter.get(position), Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getActivity().getApplicationContext(), PickUpScanList.class);
                                    i.putExtra("NAV_FORM_TYPE_VAL", "FORM_PICKUP_COMPLETE");
                                    i.putExtra("pickId", dataAdapter.get(position).getmAssignNo());
                                    i.putExtra("assignId", dataAdapter.get(position).getmAssignId());
                                    i.putExtra("companyId", dataAdapter.get(position).getmCompanyId());
                                    //i.putStringArrayListExtra("listId", list);
                                    startActivity(i);
                                }
                            } catch (Exception e) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Exception Oncardview click" + e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onButtonCancelClick(View view, int position) {

                    }

                    @Override
                    public void onButtonMapClick(View view, int position) {
                        try {
                            String shipAdd1 = addressAdapter.get(position).getmShipAdd1();
                            String shipAdd2 = addressAdapter.get(position).getmShipAdd2();
                            String shipCity = addressAdapter.get(position).getmShipCity();
                            String shipPin = addressAdapter.get(position).getmShipPin();
                            Crashlytics.log(android.util.Log.ERROR, TAG, "print shipAdd1" + shipAdd1 + shipAdd2 + shipCity + shipPin);
                            if (shipAdd1 == null && shipAdd2 == null && shipCity == null && shipPin == null) {
                                Toast.makeText(getActivity().getApplicationContext(), "Address not available" + addressAdapter.get(position), Toast.LENGTH_SHORT).show();
                            } else if (shipAdd1 != null && shipAdd2 != null && shipCity != null && shipPin != null) {
                                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/maps?daddr=" + shipAdd1 + shipAdd2 + shipCity + shipPin + "&dirflg=r"));
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception map click" + e.getMessage());
                        }
                    }

                    @Override
                    public void onButtonCallClick(View view, int position) {
                        try {
                            String shipContactnum = addressAdapter.get(position).getmShipContact();

                            Crashlytics.log(android.util.Log.ERROR, TAG, "print shipContactnum" + shipContactnum);
                            if (shipContactnum == null) {
                                Toast.makeText(getActivity().getApplicationContext(), "Contact number not available" + addressAdapter.get(position), Toast.LENGTH_SHORT).show();
                            } else if (shipContactnum != null) {
                                PhoneCall phcl = new PhoneCall();
                                phcl.call(shipContactnum, getActivity());
                            }
                        } catch (Exception e) {
                            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception map click" + e.getMessage());
                        }
                    }

                    @Override
                    public void onRVPTextClick(View view, int position) {

                    }
                };
            }
        }catch (Exception e) {
            Crashlytics.log(android.util.Log.ERROR, TAG, "Exception pickupcompletestatus" + e.getMessage());
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e(TAG, "on Start called");
        SESSION_TRANSPORT = tiny.getString("transport_val");
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            setPickUp = session.getPickUpId(session.getUserDetails().get(session.KEY_USER_NAME));
            if (setPickUp != null) {

                dataId = setPickUp.toArray(new String[setPickUp.size()]);
                cDB = new CouchBaseDBHelper(getActivity().getApplicationContext());

                for (Object ob : dataId) {
                    if (ob instanceof String) {
                        String[] sData = ((String) ob).split(":::");
                        //System.out.print(sData);
                        assign_no.add(sData[0]);
                        assign_id.add(sData[1]);
                        companyId.add(sData[7]);
                        incompleteList.add(sData[0] + "_" + sData[1] + "_" + sData[8] + "_" + mLastUpdateTime);
                        addressinfo.add(sData[0] + "_" + sData[8] + "_" + mLastUpdateTime);
                    }
                }

                for (Object ob : dataId) {
                    if (ob instanceof String) {
                        String[] sData = ((String) ob).split(":::");
                        int i = 0;
                        for (String status : incompleteList) {
                          boolean statusFlag = cDB.retrieveCompletePickUpStatus(status);
                            if (statusFlag && status.contains(sData[0])) {
                                Crashlytics.log(android.util.Log.ERROR, TAG, "Matching Assignment Id: for AllStatus " + statusFlag + "i=" + i++);
                                dataAdapter.add(new PickUpCustomData(sData[0], sData[2], "Shipper Account No: " + sData[5], "Pickup Time: " + sData[6], "No of Pcs: " + sData[9], sData[1].toString(), sData[8].toString()));
                            }
                        }
                        for (String address : addressinfo) {
                            Map<String, Object> getAddress = cDB.getShipperAdress(address);

                            if (address.contains(sData[0])) {
                                addressAdapter.add(new PickUpCustomData((String) getAddress.get("shipAdd1"), (String) getAddress.get("shipAdd2"), (String) getAddress.get("shipPin"), (String) getAddress.get("shipCity"), (String) getAddress.get("shipConct")));
                            }
                        }
                    }
                }

            }

            mAdapter = new PickUpCompleteStatusAdapter(getActivity().getApplicationContext(), dataAdapter, itemTouchListener);
            mRecyclerView.setAdapter(mAdapter);
        }catch(Exception e) {

        }

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause called");
        incompleteList.clear();
        addressinfo.clear();
        dataAdapter.clear();
        addressAdapter.clear();
    }
}



