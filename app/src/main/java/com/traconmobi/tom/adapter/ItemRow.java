package com.traconmobi.tom.adapter;

/**
 * Created by kumargaurav on 6/27/16.
 */
public class ItemRow {
    private String assignmentNo;
    private String contactNo;
    private String consigneeName;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String amount;
    private String amountType;
    private String id;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    private String reason;
    private String COD_amount;
    private String firstname;
    private String lastname;
    private String pincode;

    public String getCOD_amount() {
        return COD_amount;
    }

    public void setCOD_amount(String COD_amount) {
        this.COD_amount = COD_amount;
    }

    public ItemRow(String id, String assignmentNo, String contactNo, String consigneeName, String addressLine1, String addressLine2, String amount, String city, String amountType) {
        this.assignmentNo = assignmentNo;
        this.contactNo = contactNo;
        this.consigneeName = consigneeName;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.amount = amount;
        this.amountType = amountType;
        this.id = id;
    }

    public ItemRow(String id,String assignmentNo, String contactNo, String consigneeName, String addressLine1, String addressLine2, String amount, String city, String amountType, String reason) {
        this.assignmentNo = assignmentNo;
        this.contactNo = contactNo;
        this.consigneeName = consigneeName;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.amount = amount;
        this.amountType = amountType;
        this.id = id;
        this.reason = reason;
    }

    public ItemRow(String id,String assignmentNo, String firstname, String lastname, String addressLine1, String city, String pincode, String amount) {
        this.assignmentNo = assignmentNo;
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.addressLine1 = addressLine1;
        this.city = city;
        this.pincode = pincode;
        this.amount = amount;
    }


    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssignmentNo() {
        return assignmentNo;
    }

    public void setAssignmentNo(String assignmentNo) {
        this.assignmentNo = assignmentNo;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
